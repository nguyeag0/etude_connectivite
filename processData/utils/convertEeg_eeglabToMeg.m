function megeeg_data = convertEeg_eeglabToMeg(eeglab_data, sampling_rate, epoch_quantity)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    megeeg_data = reshape(eeglab_data, size(eeglab_data, 1), sampling_rate, epoch_quantity);
    megeeg_data = permute(megeeg_data,[2 1 3]);
end

