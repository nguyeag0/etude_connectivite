function [ laplacianClosestChannelsStruct ] = getClosestChannelsStruct( chanlocs_filename )
%GETCLOSESTELECTRODESSTRUCT For each channels get the closest channels
%   For each channels get the closest channels according to small Laplacian
%   setup
%   For each channels, considered all electrodes that are at most 30mm
%   away.
%   
%   For each channel u index i:
%   u_i^LAP = u_i - sum_jinS_i(h_ij*uj)
%   h_ij = (1/d_ij) / sum_jinS_i(1/d_ij)

%     small_laplacian_threshole = 33.5; % 33.5 is for small laplacian with 128 electrodes BIOSEMI
small_laplacian_threshole = 85; % 85 is for small laplacian with 19 electrodes BIOSEMI
cprintf('blue', 'slap threshold %d for 19 electrodes\n',  small_laplacian_threshole);

    %  Get channels locations and labels
    xyz = get_xyz_locations_n_labels(chanlocs_filename);

    %  Get matrix of channel distances
    distance_matrix = squareform(pdist(xyz.locations, 'euclidean'));
    
    % Get the closest electrode
    laplacianClosestChannelsStruct = {};
    for i=1:size(distance_matrix, 1)
        channel_i = distance_matrix(i,:);

        n = 5;
        [xs, index] = sort(channel_i);
        closest_distance = xs(1:n);
        closest_distance = closest_distance(closest_distance <= small_laplacian_threshole);
        n = length(closest_distance);
        result_index = index(1:n);
        closest_labels = xyz.labels(result_index);

        laplacianClosestChannelsStruct(i).closest_distance = closest_distance;
        laplacianClosestChannelsStruct(i).closest_labels_index = result_index;
        laplacianClosestChannelsStruct(i).closest_labels = closest_labels;
        
        closest_distance(1) = [];
        laplacianClosestChannelsStruct(i).inv_closest_distance = 1./closest_distance;
        laplacianClosestChannelsStruct(i).h_ij = laplacianClosestChannelsStruct(i).inv_closest_distance/sum(laplacianClosestChannelsStruct(i).inv_closest_distance)
        
    end

end

