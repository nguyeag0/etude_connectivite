function [ COMPS ] = get_COMPS_info( comps_path, channel_location_filename )
%GET_COMPS_INFO 
%   COMPS: 
%   column 1 and 2: possible combination with 128 pairs
%   column 3: distance between channel pair
%   column 4: label "isipsi" = 1; "iscontra" = 2; "transcallosal" = 3; regarding left lesion side
%   biosemi128.xyz location file:
%   x axis: negative toward left ear, positive toward right ear
%   y axis: negative toward the nose, positive toward back of the head
%   z axis: negative toward the neck, positive toward the top of the head
%   ispsi: channel that is on ipsilesional (left) side

xyz = xyzread(channel_location_filename, 'headerlines', 1);
load(comps_path);
COMPS = double(COMPS);
distance = pdist(xyz.locations, 'euclidean')';

% x coordinate to get the ipsilesional (left) side without the central channel is : x < -10
% Interesting distance threshold between two channels : > 60
x_ipsi_threshold = -10;
isipsi=find(xyz.locations(:,1)< x_ipsi_threshold); % to avoid the 'z' channels (Cz, Fz, etc.)
channel_ipsi_label = find(ismember( COMPS(:, 1), isipsi ) & ...
                          ismember( COMPS(:, 2), isipsi ) & ...
                          distance > 60);
                                            
% x coordinate to get the contralesional (right) side without the central channel is : x > 10
x_contra_threshold = 10;
iscontra=find(xyz.locations(:,1)> x_contra_threshold);
channel_contra_label = find(ismember( COMPS(:, 1), iscontra ) & ...
                            ismember( COMPS(:, 2), iscontra ) & ...
                            distance > 60);
              
% get transcallosal channels pairs
channel_transcallosal_label = find( ...
                            ( (ismember( COMPS(:, 1), isipsi ) & ismember( COMPS(:, 2), iscontra )) | ...
                                (ismember( COMPS(:, 1), iscontra ) & ismember( COMPS(:, 2), isipsi ))... 
                            ) & distance > 60 ...
                            );

channel_side_label = zeros(length(COMPS), 1);
channel_side_label(channel_ipsi_label) = 1;
channel_side_label(channel_contra_label) = 2;
channel_side_label(channel_transcallosal_label) = 3;

COMPS = [COMPS distance channel_side_label];


end

