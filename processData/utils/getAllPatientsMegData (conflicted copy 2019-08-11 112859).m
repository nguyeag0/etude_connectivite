function [ allPatientsMeg_struct ] = getAllPatientsMegData( current_dir, data_dir  )
%GETALLPATIENTSMEGDATA Summary of this function goes here
%   Detailed explanation goes here

    cprintf('blue', 'getAllPatientsMegData\n');

%     patients_data_path = [current_dir data_dir]; dossier data patients et
%     sujet d�placer dans C:\data\
    patients_data_path = data_dir;
    
    eeg_folderName = dir(patients_data_path);
    counter = 1;
   
    if(strcmp(data_dir, 'FC_patients_data\'))
        for item_index=1:size(eeg_folderName, 1)

            if eeg_folderName(item_index).isdir &&(size(strfind(eeg_folderName(item_index).name, 'P'), 1) > 0)
                subject_index_str = eeg_folderName(item_index).name(2:end);
                subject_index = str2num(subject_index_str);
                meg_file_path = dir([patients_data_path 'P' subject_index_str '\*.mat']);
                load([patients_data_path 'P' subject_index_str '\' meg_file_path.name]);

                cprintf('blue', 'Loading MEG data %d.\n', subject_index);
                allPatientsMeg_struct(counter).subjectindex = subject_index;
                allPatientsMeg_struct(counter).megdata = meg;
                allPatientsMeg_struct(counter).megdata.rawdata = meg.data;

                counter = counter+1;
            end  
        end
    elseif(strcmp(data_dir, 'etude2_data\'))
        load([ patients_data_path 'allPatientsMeg.mat']);
        
        for item_index=1:length(allPatientsMeg)
            cprintf('blue', 'Loading MEG data Patient %d.\n', item_index);
            allPatientsMeg_struct(counter).subjectindex = item_index;
            allPatientsMeg_struct(counter).megdata = allPatientsMeg(item_index);
            allPatientsMeg_struct(counter).megdata.rawdata = allPatientsMeg(item_index).data;
            counter = counter + 1;
        end
    elseif(strfind(data_dir, 'Data_Anais_Leslie\'))
        for item_index=1:size(eeg_folderName, 1)

            if eeg_folderName(item_index).isdir &&(size(strfind(eeg_folderName(item_index).name, 'S'), 1) > 0)
                subject_index_str = eeg_folderName(item_index).name(2:end);
                subject_index = str2num(subject_index_str);
                meg_file_path = dir([patients_data_path 'S' subject_index_str '\*.mat']);
                if(size(meg_file_path, 1) >0)
                    load([patients_data_path 'S' subject_index_str '\' meg_file_path.name]);
                    cprintf('blue', 'Loading MEG data %d.\n', subject_index);
                    allPatientsMeg_struct(counter).subjectindex = subject_index;
                    allPatientsMeg_struct(counter).megdata = meg;
                    allPatientsMeg_struct(counter).megdata.rawdata = meg.data;
                    counter = counter+1;
                else
                    cprintf('blue', 'No MEG data %d.\n', subject_index);
                end
                
            end  
        end
    end
    
end

