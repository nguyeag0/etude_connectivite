function [refAddedData] = addReference204ToMegData(megdata)
%ADDREFERENCE204TOMEGDATA Summary of this function goes here
%   Detailed explanation goes here

refAddedData = megdata.data;

refAddedData = cat(2, refAddedData, zeros(megdata.srate, 1, size(megdata.data, 3)));

end

