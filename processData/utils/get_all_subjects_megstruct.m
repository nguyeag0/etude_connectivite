function [ allSubjects_megStruct_interpoledNorerefNoswap, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct( data_dir, current_dir, origine_chanloc_file_path )
%GET_ALL_PATIENTS_MEGSTRUCT Get all patients megs struct according to
%options (interpolation, rereferencing, swap)
%   Detailed explanation goes here

% Interpolation 1 | Reref 'no' | Swap 0
allSubjects_megStruct_interpoledNorerefNoswap = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path);
allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref = struct(allSubjects_megStruct_interpoledNorerefNoswap);

% Load the wanted channels and index
load('data/channels19_1020_biosemi.mat');
xyz = xyzread(origine_chanloc_file_path, 'headerlines', 1);
channels19_index = find(ismember(xyz.labels, channels19_1020_biosemi.biosemi));

% Get the 19 EEG channels and reref avg on 19 channels
for i = 1:length(allSubjects_megStruct_interpoledNorerefNoswap)
    data = allSubjects_megStruct_interpoledNorerefNoswap(i).megdata.data;
    data19 = data(:,channels19_index,:);
    
    allSubjects_megStruct_interpoledNorerefNoswap(i).megdata.data19 = data19;
    allSubjects_megStruct_interpoledNorerefNoswap(i).megdata.channels19 = channels19_index;
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i) = allSubjects_megStruct_interpoledNorerefNoswap(i);
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.allInterpolatedData = allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data;
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data = data19;
    megdata = allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata;
    megdata = nut_eegref(megdata, 'AVG');
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata = megdata;
end

% save('data/allSubjects_megStruct_interpoledNorerefNoswap.mat', 'allSubjects_megStruct_interpoledNorerefNoswap', '-v7.3');
% save('data/allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref.mat', 'allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref', '-v7.3');
end

