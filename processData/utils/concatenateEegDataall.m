function dataall = concatenateEegDataall(allParticipantsMeg)
%CONCATENATEEEGDATAALL All EEG in 3D (srate * channels * epochs) 512*19*300
%concatenated in one variable in 2D (512*300) * 19
%   allParticipantsMeg: struct of all the participants' meg
%   dataall: concatenation of the EEG of all the participants in 2D
%   epochs * channels


dataall=[];
for i = 1:length(allParticipantsMeg)
    
    % Downsample EEG data if meg.srate = 1024
    if allParticipantsMeg(i).megdata.srate>512
        allParticipantsMeg(i).megdata = nut_downsample(allParticipantsMeg(i).megdata,2);
    elseif( mod(allParticipantsMeg(i).megdata.srate, 512) ~= 0)
        cprintf('blue', 'Participant #%d has a sampling rate at %d\n.', i, allParticipantsMeg(i).megdata.srate);
    end
    data19 = allParticipantsMeg(i).megdata.data;
    data2D = reshape( permute(data19,[1 3 2]), ...
                   [size(data19, 1)*size(data19, 3) size(data19, 2) ]);     % reshape [1024*300 19] or [512*300 19]     
    dataall = cat(1,dataall,zscore(data2D));
end

end

