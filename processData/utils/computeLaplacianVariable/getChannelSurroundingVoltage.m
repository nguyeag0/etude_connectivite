function [ S_i ] = getChannelSurroundingVoltage( channel_i, surrounding_i, data_channel_i, meg )
%GETCHANNELSURROUNDINGVOLTAGE Summary of this function goes here
%   surrounding_i : Surrounding Channels informations of the considered
%   channel
%   channel_i : index of considered channel
%   j : channel index j in the surrounding of channel index i
% 
%   For each channel u index i:
%   u_i^LAP = u_i - S_i
%   S_i = sum_jinS_i(h_ij*uj)
%   u_i^LAP = u_i - sum_jinS_i(h_ij*uj)
%   h_ij = (1/d_ij) / sum_jinS_i(1/d_ij)
% 
%   Here we compute : S_i = sum_jinS_i(h_ij * uj)

    surrounding_i.closest_distance(1) = [];
    surrounding_i.closest_labels_index(1) = [];
    surrounding_i.closest_labels(1) = [];

    % Compute denominator
    denominator = sum(surrounding_i.inv_closest_distance);
    S_i = 0;
    for surrounding_i_channel_j=1:length(surrounding_i.closest_distance)
        channel_j = surrounding_i.closest_labels_index(surrounding_i_channel_j);
        data_index_j = find(meg.goodchannels == channel_j);
        u_j = data_channel_i(data_index_j);
        numerator = 1/surrounding_i.closest_distance(surrounding_i_channel_j);
      
       h_ij = numerator / denominator;
       S_i = S_i +  h_ij * u_j;        
       
    end


end

