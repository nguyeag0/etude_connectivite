function [ zScore_matrix, ICmatrix ] = get_zScore_n_imagCoh_allPatients_matrix( allPatientsMeg_struct, COMPS )
%GET_ZSCORE_MATRIX get the zScore and imaginary matrix for all the patients
%   zScore_matrix: 8128 x 10, Z score of all the pairs combination by all
%   the 10 patients. Normalization of the imaginary coherence values.
%   ICmatrix: 8128 x 10, imaginary coherence of all pairs by all the 10 patients
 
    zScore_matrix = nan(length(COMPS),10);
    ICmatrix = nan(length(COMPS),10);
    for index=1:length(allPatientsMeg_struct)
        [index_zScore,imaginary_coherence, subject_comps] = get_index_zScore( allPatientsMeg_struct, index );
        
        subject_comps_match = ismember(COMPS, subject_comps);
        subject_comps_match_index = find(all(subject_comps_match==1, 2));
        
        ICmatrix(subject_comps_match_index,index) = imaginary_coherence;
        zScore_matrix(subject_comps_match_index, index) = index_zScore;
    end
end

