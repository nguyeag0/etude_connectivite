function [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore( allPatientsMeg_struct, index )
%GET_INDEX_ZSCORE Summary of this function goes here
  
    meg = allPatientsMeg_struct(index).megdata;

    % Get coherence matrix between all electrodes. With 'all', it is all
    % combinations (pairs) among all existing electrodes coherence with the rest of
    % the electrodes
    SC = fcm_sensorccohere31(meg.srate,meg.data,'all','b',[8 12]);

    % Get patient imaginary coherence
    index_imaginary_coherence = abs(imag(SC.coh));
    index_zScore = (index_imaginary_coherence - mean(index_imaginary_coherence) ) / std(index_imaginary_coherence); % Z score : normalised data
    subject_comps = SC.comps;
end

