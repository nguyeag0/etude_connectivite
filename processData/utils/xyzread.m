function xyz = xyzread(filename,varargin)
% xyzread simply imports the x,y,z columns of a .xyz file.  Note: there
% is no real standard for .xyz files, so your .xyz file may be different
% from the .xyz files I wrote this for.  I wrote this one for GMT/GIS
% files.  
% Modified script dung.nguyen@unige.ch 02/08/2018

%% Syntax
% 
% [x,y,z] = xyzread(filename)
% [x,y,z] = xyzread(filename,Name,Value) 
% 
%% Description
% 
% [x,y,z] = xyzread(filename) imports the columns of a plain .xyz file. 
% 
% [x,y,z] = xyzread(filename,Name,Value) accepts any textscan arguments 
% such as 'headerlines' etc. 
% 
%% Author Info 
% This script was written by Chad A. Greene of the University of Texas 
% at Austin's Institute for Geophysics (UTIG), April 2016. 
% http://www.chadagreene.com 
% 
% See also xyz2grid and textscan. 

%% Error checks: 

narginchk(1,inf) 
nargoutchk(1,1)
assert(isnumeric(filename)==0,'Input error: filename must ba a string.') 
assert(exist(filename,'file')==2,['Cannot find file ',filename,'.'])

%% Open file: 

fid = fopen(filename); 
T = textscan(fid,'%f %f %f %s',varargin{:});
xyz.locations = [T{1} T{2} T{3}];
xyz.labels = T{4};
fclose(fid);

%% Get scattered data: 
% 
% x = T{1}; 
% y = T{2}; 
% z = T{3}; 

end