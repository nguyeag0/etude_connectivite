function [ output_args ] = get_19_channels_COMPS_info( origine_chanloc_file )
%GET_19_CHANNELS_COMPS_INFO Summary of this function goes here
%   COMPS19 :
%   Column 1 - 2 : all possible combinations on 19 channels (index from 1 to
%   19)
%   Column 3 - 4 : all possible combinations with 'real' indexes

load('data/channels19_1020_biosemi.mat');

% Get all combination of 19 electrodes
COMPS19 = combnk(1:19, 2);

% Get labels and locations of the 128 electrodes BIOSEMI
xyz = xyzread(origine_chanloc_file_path, 'headerlines', 1);

% Get the index of the 19 electrodes in the 128 indexing
channels_ind = find(ismember(xyz.labels, channels19_1020_biosemi.biosemi));

% Get labels and locations of the 19 electrodes BIOSEMI
xyzloc19 = xyz.locations(channels_ind,:);
distance = pdist(xyzloc19, 'euclidean')';

% Get channel type information 
x_left_threshold = -10;
isleft = find(xyzloc19(:,1)< x_left_threshold); % to avoid the 'z' channels (Cz, Fz, etc.)
x_right_threshold = 10;
isright = find(xyzloc19(:,1)> x_right_threshold);
iscentral = find(xyzloc19(:,1)> x_left_threshold & xyzloc19(:,1) < x_right_threshold);

channels_type_list = zeros(length(channels_ind), 1) - 1;
channels_type_list(isleft) = 1;
channels_type_list(isright) = 2;
channels_type_list(iscentral) = 0;

channels_info = [channels_ind channels_type_list];

% Get pair type information
pair_left_label = find( ismember(COMPS19(:,1), isleft ) & ismember(COMPS19(:,2), isleft) | ...
                        ismember(COMPS19(:,1), isleft ) & ismember(COMPS19(:,2), iscentral) | ...
                        ismember(COMPS19(:,1), iscentral ) & ismember(COMPS19(:,2), isleft) );

pair_right_label = find( ismember(COMPS19(:,1), isright ) & ismember(COMPS19(:,2), isright) | ...
                        ismember(COMPS19(:,1), isright ) & ismember(COMPS19(:,2), iscentral) | ...
                        ismember(COMPS19(:,1), iscentral ) & ismember(COMPS19(:,2), isright) );
 
% get transcallosal channels pairs
x_ipsi_threshold = -10;
isipsi = find(xyzloc19(:,1)< x_ipsi_threshold); % to avoid the 'z' channels (Cz, Fz, etc.)
x_contra_threshold = 10;
iscontra = find(xyzloc19(:,1)> x_contra_threshold);
pair_transcallosal_label = find( ...
                            ( (ismember( COMPS19(:, 1), isipsi ) & ismember( COMPS19(:, 2), iscontra )) | ...
                                (ismember( COMPS19(:, 1), iscontra ) & ismember( COMPS19(:, 2), isipsi ))... 
                            ) ...
                            );

channel_side_label = zeros(length(COMPS19), 1);
channel_side_label(pair_left_label) = 1; % -1 to visualize
channel_side_label(pair_right_label) = 2; % 0 to visualize
channel_side_label(pair_transcallosal_label) = 3;% 1 to visualize

COMPS19(:,3:end) = [];
COMPS_ind = [channels_ind(COMPS19(:,1)) channels_ind(COMPS19(:,2))];
% COMPS_ind = [channels_ind(COMPS19(:,1)) channels_ind(COMPS19(:,2)) COMPS19(:,3)];
COMPS19 = [COMPS19 COMPS_ind(:,1:2) channel_side_label distance];
save('data/COMPS19.mat', 'COMPS19');

% TOPOPLOT

selected_pairs = COMPS_ind(:,1:2);    
values = COMPS_ind(:,3);
% selected_pairs = COMPS_ind(pair_transcallosal_label,1:2);    
% values = COMPS_ind(pair_transcallosal_label,3);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
title('Pair involve');   

%% Get the symmetric of the channels
% channels19 : col1 col2 col3 col4 col5 col6 col7 col8 --> id_chan1 sur 19 de la
% paire | id_chan2 sur 19 de la paire | id_chan1 sur 128 de la paire |
% id_chan2 sur 128 de la paire | idem en symmétrique

% load sensor_labels
load('data\symmetricalLabelsBiosemi.mat');
load('data\channels19_1020_biosemi.mat');
COMPS19_tmp = COMPS19;
channels19 = COMPS19_tmp(:,1:4);
channels19(:,5) = cell2mat(sensor_labels(channels19(:,3), 3));
channels19(:,6) = cell2mat(sensor_labels(channels19(:,4), 3));
channels19(:,7) = zeros(length(channels19), 1);
channels19(:,8) = zeros(length(channels19), 1);
channels19(:,9) = zeros(length(channels19), 1);

idnum = length(channels19(:,5));
for i=1:idnum
    channels19(i,7) = find( channels19(i,5)==channels19_1020_biosemi.index );
    channels19(i,8) = find( channels19(i,6)==channels19_1020_biosemi.index );
    channels19(i,9) = find( (channels19(i,7)==channels19(:,1)) & (channels19(i,8)==channels19(:,2)) |...
                    (channels19(i,7)==channels19(:,2)) & (channels19(i,8)==channels19(:,1)));
end

COMPS19 = channels19;


% Add distance of the pairs in COMPS19 10th column
COMPS19 = [COMPS19 distance];

save('data/COMPS19.mat', 'COMPS19');
   
end

