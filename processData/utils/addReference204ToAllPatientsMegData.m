function [allPatientsMeg_struct] = addReference204ToAllPatientsMegData(allPatientsMeg_struct)
%ADDREFERENCE204TOALLPATIENTSMEGDATA Summary of this function goes here
%   Detailed explanation goes here

for index=1:size(allPatientsMeg_struct, 2)    
    refAddedData = addReference204ToMegData(allPatientsMeg_struct(index).megdata);
    allPatientsMeg_struct(index).megdata.data = refAddedData;
end
    
end

