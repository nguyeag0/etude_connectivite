function [ IC_3pairs_matrix, Zmatrix ] = get_pseudoZScore_n_imagCoh_6channels_allPatients_matrix( allPatientsMeg_struct, COMPS, pair_indexes )
%GET_PSEUDOZSCORE_N_IMAGCOH_6CHANNELS_ALLPATIENTS_MATRIX Summary of this function goes here
%   Z-score (La cote Z correspond au nombre d'�carts types s�parant un r�sultat de la moyenne)

    IC_3pairs_matrix = nan(length(pair_indexes),length(allPatientsMeg_struct));
%     pseudo_Zscore_matrix = nan(length(pair_indexes),length(allPatientsMeg_struct));
    pseudo_Zscore_matrix = nan(1,length(allPatientsMeg_struct));

    for index=1:length(allPatientsMeg_struct)
        data = allPatientsMeg_struct(index).megdata.data;
        SC = fcm_sensorccohere31(512,data,[COMPS(pair_indexes(1),1) COMPS(pair_indexes(1),2);...
                                       COMPS(pair_indexes(2),1) COMPS(pair_indexes(2),2);...
                                       COMPS(pair_indexes(3),1) COMPS(pair_indexes(3),2)],...
                                        'b',[8 12]);
        IC_3pairs = abs(imag(SC.coh));
        IC_3pairs_matrix(:,index) = IC_3pairs;
        
        pseudo_Zscore = IC_3pairs(3) - mean(IC_3pairs(1:2));
        Zmatrix(:,index) = pseudo_Zscore;        
        
%         pseudo_Zscore = IC_3pairs(3) - mean(IC_3pairs(1:2));
%         pseudo_Zscore_matrix(:,index) = repmat(pseudo_Zscore, 3, 1);
%         pseudo_Zscore_matrix(:,index) = IC_3pairs(3) - mean(IC_3pairs(1:2));
        %pseudo_Zscore_matrix(:,index) = IC_3pairs(1) - mean(IC_3pairs(2:3)); % pair_ipsi - mean(pair_contr, pair_trans)
    end

end

