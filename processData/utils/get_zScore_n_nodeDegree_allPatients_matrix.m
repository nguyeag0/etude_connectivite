function [ nodeDegree_zScore, node_degree  ] = get_zScore_n_nodeDegree_allPatients_matrix( ICM )
%GET_ZSCORE_N_NODEDEGREE_ALLPATIENTS_MATRIX 
%   ICM: imaginary coherence matrix in channels x channels x patients
% node_degree : mean of imaginary coherence of a channel over all the 127 other, calculated per all patients
% node_degree_zScore : node_degree normalize by patient.

    node_degree = squeeze(nanmean(ICM));

    nodeDegree_zScore=zeros(size(node_degree)); 
    for k=1:10
        nodeDegree_zScore(:,k)=(node_degree(:,k) - nanmean(node_degree(:,k))) ./ nanstd(node_degree(:,k));
%         nodeDegree_zScore(:,k)=(node_degree(:,k) - mean(node_degree(:,k))) ./ std(node_degree(:,k));
    end

end

