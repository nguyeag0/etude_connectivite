function [ refAddedData ] = addA1ReferenceToMegData( megdata )
%ADDA1REFERENCETOMEGDATA Summary of this function goes here
%   Detailed explanation goes here

    refAddedData = megdata.data;
    refAddedData = cat(2, ...
               zeros(megdata.srate, 1, size(megdata.data, 3)), refAddedData);
    
end

