function [ pair_indexes ] = getThreePairs( COMPS, considered_pairs, method, searched_pair )
%GETTHREEPAIRS Summary of this function goes here
%   Method: 'good_pfdr' | 'random'  |  'surrounding_pairs'
%  If Method 'random' chosen : 1 = 'searched pair 1 (ipsi) | 2 = 'searched pair 2' (contra) | 3 = 'searched pair 3 (trans)
%   Fixed_pair : best pair of previous cycle
    
    ipsi_pairs = find(COMPS(:,4)==1);
    contra_pairs = find(COMPS(:,4)==2);
    trans_pairs = find(COMPS(:,4)==3);
            
    if(strcmp(method, 'good_pfdr'))
        cprintf('blue', 'Method good_pfdr.\n');
        pair_indexes = considered_pairs;
        
    elseif(strcmp(method, 'random'))
        cprintf('blue', 'Method randomly.\t');
        if nargin < 4
            warning('\nRandom method chosen, please give the pair type to search for (1 | 2 | 3).\n');
        else
            cprintf('blue', 'Searched pair %d.\n', searched_pair);
            if(searched_pair == 0)
                random_ipsi_pair = ipsi_pairs( randi( length(ipsi_pairs) ) );
                random_contra_pairs = contra_pairs( randi( length(contra_pairs)));
                random_trans_pairs = trans_pairs( randi( length(trans_pairs)));
                pair_indexes = [random_ipsi_pair random_contra_pairs random_trans_pairs];    
            elseif(searched_pair == 3)
                random_trans_pairs = trans_pairs( randi( length(trans_pairs)));
                pair_indexes = [considered_pairs(1) considered_pairs(2) random_trans_pairs];
            elseif(searched_pair == 2)
                random_contra_pairs = contra_pairs( randi( length(contra_pairs)));
                pair_indexes = [considered_pairs(1) random_contra_pairs considered_pairs(3)];    
            elseif(searched_pair == 1)
                random_ipsi_pair = ipsi_pairs( randi( length(ipsi_pairs) ) );
                pair_indexes = [random_ipsi_pair considered_pairs(2) considered_pairs(3)];    
            end
        end
        
    elseif(strcmp(method, 'surrounding_pairs'))
        cprintf('blue', 'ThreePairs in the considered surrounding.\n'); 
    else
        cprintf('blue', 'No ThreePairs given.\n'); 
    end
end

