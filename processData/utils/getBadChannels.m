function bad_channels = getBadChannels(meg)
    total_channels = size(meg.sensor_labels, 1);
    total_good_channels = length(meg.goodchannels);
    total_bad_channels = total_channels - total_good_channels;
    percentage_bad_channels = total_bad_channels*100/total_channels;
    
    if ( percentage_bad_channels > 10 )
         warning('%.2f %% of bad electrodes', percentage_bad_channels);
    end
    
    total_channels_index = (1:total_channels)';
    bad_channels = setdiff(total_channels_index, meg.goodchannels);
end