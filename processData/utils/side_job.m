%% Get new indexes of the sensor_labels

patient_data_path = 'C:\Users\dunn\switchdrive\Documents\projects\3eEtude_connectivite\FC_patients_data\P01\';
patient_data_filename = 'filtdata_S_rest_pre_300_ellip2pass_1to20Hz';

load([patient_data_path patient_data_filename]);
load('symmetricalLabelsBiosemi.mat');

data = meg.data;
eeglab_data = convertEeg_megToEeglab(data);

new_index_cellarray = cell(128, 1);

for index=1:128
    disp(sensor_labels(index,2))
    new_index_cellarray{index} = find(strcmp(sensor_labels(:,1), sensor_labels(index,2)));
end

sensor_labels(:,3) = new_index_cellarray;

% Testing the matching new index
for index=1:128
    assert(strcmp(sensor_labels(index, 2), sensor_labels(sensor_labels{index, 3}, 1)) );
end

save('symmetricalLabelsBiosemi.mat', 'sensor_labels');

%% Get the lesion side of patient
% create and manage struct https://blogs.mathworks.com/loren/category/structures/
patient_index = [1 2 4 5 6 8 10 11 12 13];
N = length(patient_index);

lesion_side = ['D' 'G' 'G' 'D' 'G' 'D' 'G' 'D' 'G' 'D'];
N = length(lesion_side);

subject_lesion_side = struct([])

for i=1:N
    subject_lesion_side(i).patient_index = patient_index(i);
    subject_lesion_side(i).lesion_side = lesion_side(i);
end

save('subject_lesion_side.mat', 'subject_lesion_side');