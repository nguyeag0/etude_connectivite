function allParticipantsMeg = dividePCAdataToAllParticipant(allParticipantsMeg, PCAdataall, data19)
%DIVIDEPCADATATOALLPARTICIPANT Summary of this function goes here
%   From PCA signal (i.e. PCAsignal = eegsignal * PCAcoeff)concatenated
%   from all the participants together. Divide by the participants and
%   store back in each participants' MEG
%   Here, eegdata reduced to 19 channels

for i = 1:length(allParticipantsMeg)
    data2D = PCAdataall(1:512*300,:);
    PCAdataall(1:512*300,:)=[];
    data3D = reshape(data2D, size(data19, 1), size(data19, 3), size(data19, 2));
    data3D = permute(data3D,[1 3 2]);
    
    allParticipantsMeg(i).megdata.data = data3D;
    allParticipantsMeg(i).megdata.dataPCA = data3D;
end
end

