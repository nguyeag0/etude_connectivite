function [] = topoplot_individualPairedChannels(pair_COMPS, pair_indexes, betas_current, origine_chanloc_file_path, plot_title)
%TOPOPLOT_PAIREDCHANNELS topoplot on 2D EEG cap the wanted paired-channels
%with their betas respective values
%   Detailed explanation goes here

selected_pairs = pair_COMPS(pair_indexes,:);    
values = betas_current;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];

% figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
title(plot_title);   


end

