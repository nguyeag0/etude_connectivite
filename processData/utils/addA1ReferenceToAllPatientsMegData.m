function [ allPatientsMeg_struct ] = addA1ReferenceToAllPatientsMegData( allPatientsMeg_struct )
%ADDA1REFERENCETOALLPATIENTSMEGDATA Summary of this function goes here
%   Detailed explanation goes here

    for index=1:size(allPatientsMeg_struct, 2)
        cprintf('blue', 'Add reference A1 to EEG patient %d \n.', index);
        refAddedData = addA1ReferenceToMegData(allPatientsMeg_struct(index).megdata);
        allPatientsMeg_struct(index).megdata.data = refAddedData;
%         allPatientsMeg_struct(index).megdata.goodchannels = 1:size(refAddedData,2);
    end
end

