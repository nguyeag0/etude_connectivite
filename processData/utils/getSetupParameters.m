function parameters = getSetupParameters()
%GETSETUPPARAMETERS ask users for the wanted : interpolating data,
%re-referencing data, swap data on left lesion side

    prompt = 'Interpolate data? (0=no, 1=yes) ';
    parameters.interpolate_data = input(prompt);
    if(isempty(parameters.interpolate_data))
        parameters.interpolate_data = 1;
    end
    fprintf('Interpolate data %d\n', parameters.interpolate_data);

    prompt = 'Re-referencing data? (''XX''=electrode, ''avg'',  ''slap''=small laplacian, ''no''=no )';
    parameters.rereference_data = input(prompt, 's');
    if(isempty(parameters.rereference_data))
        parameters.rereference_data = 'avg';
    end
    fprintf('New reference electrode: %s\n', parameters.rereference_data);

    prompt = 'Swapping electrodes on left lesion side? (0=no, 1=left, 2=right)';
    parameters.swap_data = input(prompt);
    if(isempty(parameters.swap_data))
        parameters.swap_data = 1;
    end
    fprintf('Swap data %d\n', parameters.swap_data);
    
%     prompt = 'consider imag coherence or zscore (ie normalize or not)? '

    
end

