function [ swapped_meg ] = swapChannelsData( original_meg, sensor_labels )
%SWAPCHANNELSDATA Swap channels data according to cap label
%   Swap channels in needed case to have the same electrodes lesion side.
    
    max_channelnum_eegcap = 128;
    swapped_meg = original_meg;
    data = original_meg.data;
    good_channels = swapped_meg.goodchannels;
    matched_good_channels = cell2mat(sensor_labels(good_channels, 3));
    sorted_matched_good_channels = sort(matched_good_channels);
      
    swapped_data = zeros(size(data));    
    
    if(size(data, 2) == max_channelnum_eegcap)
        cprintf('blue', 'swapChannelsData: swapping full %d channels.\n', size(data, 2));

        for index = 1:length(good_channels)
            swapped_data(:,index,:) = data(:,sensor_labels{good_channels(index), 3}, :);     
        end
            
    elseif(size(data, 2) < max_channelnum_eegcap)
        cprintf('blue', 'swapChannelsData: swapping %d channels.\n', size(data, 2));         
      
        for index=1:length(good_channels)  
            data_line_index = find(good_channels == find([sensor_labels{:,3}] == sorted_matched_good_channels(index) ) );
            swapped_data(:, index, :) = data(:, data_line_index,: );
        end
        corresponding_swapped_good_channels = sensor_labels(good_channels,3);
        corresponding_swapped_good_channels = sort(cell2mat(corresponding_swapped_good_channels));      
        swapped_meg.goodchannels_rawdata = good_channels;
        swapped_meg.goodchannels = corresponding_swapped_good_channels;
    else
        error('Error, swap channels could not be proceed');
    end
    
    swapped_meg.swapped_data = swapped_data;

end
