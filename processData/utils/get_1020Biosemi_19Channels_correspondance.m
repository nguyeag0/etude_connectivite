function [ output_args ] = get_1020Biosemi_19Channels_correspondance(  )
%GET_1020BIOSEMI_19CHANNELS_CORRESPONDANCE Get the interesting 19 channels
%of 10-20 convention and its correspondance with Biosemi label
%   Detailed explanation goes here

filename = 'data/19_10-20pairs.csv';
fid = fopen(filename);
data=textscan(fid, '%s %s %d %d %f',...
       'headerlines', 1,...
       'delimiter',';',...
       'TreatAsEmpty','NA',...
       'EmptyValue', NaN);                              
fclose(fid);
temp1 = data(1);
temp2 = data(2);
temp3 = data(3);
temp4 = data(4);
channels19_1020_biosemi = struct();
channels19_1020_biosemi.tentwenty = temp1{1};
channels19_1020_biosemi.biosemi = temp2{1};
channels19_1020_biosemi.index = temp3{1};
channels19_1020_biosemi.type = temp4{1};

save('data/channels19_1020_biosemi.mat', 'channels19_1020_biosemi');

end

