function PB=binomstat2(typ,varargin)

% BINOMSTAT  calculates significance matrices from multi-subject statistics.
%
% Usage (either one of the 3 following):
% PB = binomstat2('p', stats_p_3D, alpha)
% PB = binomstat2('accept',stats_accepted_3D, alpha)
% PB = binomstat2('thres', data_3D, data_threshold, alpha)
%
% STATS_P_3D        is an array (x*y*subject) with p-values.
% STATS_ACCEPTED_3D is an array (x*y*subject) with 1's for significant data
%                   points and 0 for not significant data points.
% ALPHA             a scalar indicating the significance level (eg 0.05).
% DATA_3D           is a 3D data array (x*y*subject).
% DATA_THRESHOLD    is the data threshold obtained after applying a bootstrap
%                   procedure with the timef() function of EEGLAB.
%
% (c) 2006  Adrian G. Guggisberg
%
% Reference:
% - Onton J, Delorme A, Makeig S. Frontal midline EEG dynamics during working memory. 
%   Neuroimage 2005; 27: 341-356.
%

[frqs,len,N] = size(varargin{1});

switch typ
case 'thres'
    ersp3=varargin{1};
    %ersp3=permute(ersp3,[2 1 3]);
    stat=varargin{2};
    alpha=varargin{3};
    
    threslow = repmat(stat(:,1),[1 len N]);
    threshigh= repmat(stat(:,2),[1 len N]);
    S = (ersp3>=threshigh | ersp3<=threslow);
    s = sum(S,3);
    
case 'p'
    stat=varargin{1};
    alpha=varargin{2};    
    
    S = (stat<=alpha);
    s = sum(S,3);
    
case 'accept'
    stat=varargin{1};
    alpha=varargin{2};      

    s=sum(stat,3);
end


for c1=1:len
    for c2=1:frqs
        k = s(c2,c1);
        %PB(c2,c1) = nchoosek(N,k) * alpha^k * (1 - alpha^(N-k));
        PB(c2,c1) = prod(2:N) .* alpha^k .* (1 - alpha^(N-k)) ./ ( prod(2:k) .* prod(2:N-k) );
    end
end
