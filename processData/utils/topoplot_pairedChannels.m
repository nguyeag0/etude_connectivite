function [] = topoplot_pairedChannels(pair_COMPS, pair_indexes,correlation_result, origine_chanloc_file, plot_title)
%TOPOPLOT_PAIREDCHANNELS topoplot on 2D EEG cap the wanted paired-channels
%with their respective values
%   Detailed explanation goes here

selected_pairs = pair_COMPS(pair_indexes,:);    
values = repmat(correlation_result, 3, 1);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];

figure;
topoplot_connect(display_structure, origine_chanloc_file); 
title(plot_title);   


end

