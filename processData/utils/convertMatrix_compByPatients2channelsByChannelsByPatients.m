function [ outputMatrix ] = convertMatrix_compByPatients2channelsByChannelsByPatients(inputMatrix, COMPS)
%CONVERTMATRIX_COMPBYPATIENTS2CHANNELSBYCHANNELSBYPATIENTS(ICMATRIX, COMP); Summary of this function goes here
%   Detailed explanation goes here

    outputMatrix = nan(128,128,10);
    for s=1:10
        for k=1:8128
            outputMatrix(COMPS(k,1),COMPS(k,2),s)=inputMatrix(k,s);
            outputMatrix(COMPS(k,2),COMPS(k,1),s)=inputMatrix(k,s);
        end
    end

end

