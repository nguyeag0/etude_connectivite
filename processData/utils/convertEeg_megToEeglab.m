function [eeglab_data] = convertEeg_megToEeglab(megeeg_data)
%CONVERTEEG_MEGTOEEGLAB Summary of this function goes here
%   Detailed explanation goes here

    eeglab_data=permute(megeeg_data,[2 1 3]);
    eeglab_data=reshape(eeglab_data,[size(eeglab_data,1),size(eeglab_data,2)*size(eeglab_data,3)]);
      
end

