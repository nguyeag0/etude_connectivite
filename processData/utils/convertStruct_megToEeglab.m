function eeglab_struct = convertStruct_megToEeglab( meg, origine_chanloc_file_path )
%NEWEEGLABSTRUCT Summary of this function goes here
%   Detailed explanation goes here

    eeglab_struct = eeg_emptyset();
    eeglab_struct.data = convertEeg_megToEeglab(meg.data);
    eeglab_struct = eeg_checkset(eeglab_struct);
    eeglab_struct.nbchan = size(meg.data, 2);
    eeglab_struct.srate = meg.srate;
    eeglab_struct.xmin = min(min(eeglab_struct.data));
    eeglab_struct.xmax = max(max(eeglab_struct.data));
    
    channels_location=readlocs(origine_chanloc_file_path,'filetype','custom','format',{'-Y','X','Z','labels'},'skiplines',1);
    eeglab_struct.chanlocs = channels_location;

end

