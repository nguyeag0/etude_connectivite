%% final script matlab: script&debug
%Leslie


%VRP + rest, 4-5 subjects (already cleaned)
% 1) grand average of visual task epochs (-100/+900, downsampled 125 or 250 Hz)
% (going through good epoch concatenation, individual average to group average)
% 2) microstate estimation on concatenated ~1-2 min rest from each subject,
% estimate optimal number of clusters
% 3) microstate estimation on grand average from visual task, with optimal
% estimate
% 4) pair rest/task microstates based on max correlation (if above 0.9)
% 5) backfit grand average and plot:
% figure with 3 rows: top row butterfly plot of grand average
% middle row: grand average GFP with colored areas for the labels of
% microstates that have pairs in the rest analysis (point 4)
% bottom row: the microstates topographies from point 4.

%%
    clear all;
    close all;

% 
    workingDIR = 'J:\Matlab_course\myDATA\';
    cd(workingDIR);

    origlocfile='J:\Matlab_course\biosemi128.xyz';
    chl=readlocs(origlocfile,'filetype','custom','format',{'-Y','X','Z','labels'},'skiplines',1);

    rest_filelist=dir([workingDIR 'filtdata*']);
    rest_path_list=cellfun(@strcat, repmat({workingDIR},size(rest_filelist,1),1),{rest_filelist.name}','uni',0);
    N=length(rest_path_list);

    task_filelist=dir([workingDIR '*triggB.mat']);
    task_path_list=cellfun(@strcat, repmat({workingDIR},size(task_filelist,1),1),{task_filelist.name}','uni',0);

    
%% 1)grand average of visual task epochs (-100/+900, downsampled 125 or 250 Hz)
% (going through good epoch concatenation, individual average to group average)
%looping for 4 subjects

for i=1:N
% loading data

    fprintf('loading task data...\n');
    load(task_path_list{i});
    
    mydata=eeg_emptyset;
    data=permute(meg.data,[2 1 3]);
    data=reshape(data,[size(data,1),size(data,2)*size(data,3)]);
    %data=reshape(meg.data,[size(meg.data,1)*size(meg.data,3),size(meg.data,2)])';
    
% Cleaning - eliminate DC, Bandpass 1-40, average referencing, downsampling    
    fprintf('\neliminate DC...\n'); 
    data = data - repmat(mean(data,2),1,size(data,2)); 
    
    fprintf('bandpass filtering...\n');
    filtDESIGN=designfilt('bandpassfir','FilterOrder',20, 'CutoffFrequency1',1,'CutoffFrequency2',40,'SampleRate',1000);
    data=filtfilt(filtDESIGN,data);
    
    fprintf('average referencing...\n');   
    data=data-repmat(mean(data,1),size(data,1),1);
   
    fprintf('downsampling to 250Hz...\n');
    new_fs=256;
    [p,q] =rat(new_fs/meg.srate, 0.0001);
    data=resample(double(data'),p,q)';
    mydata.srate=new_fs;
    mydata.pnts=size(data,2);
    mydata.chanlocs = chl;
    mydata.data=data;clear data
    

% bad electrodes interpolation
    fprintf('electrodes interpolation...\n');
    badel=[1,setdiff(2:128,meg.goodchannels)];
    fulldata=zeros(128,size(mydata.data,2));
    fulldata(meg.goodchannels,:)=mydata.data;
    mydata.data=fulldata; clear fulldata
    
    eeg_checkset(mydata);
    mydataC= myinterp(mydata,badel,1);
    
% epoch analysis
    fprintf('epochs analysis...\n');
    GoodEpochs=reshape(mydataC.data,[size(mydataC.data,1),mydataC.srate*2,size(mydataC.data,2)/(mydata.srate*2)]);
    
    w1=26;
    w2=224;
    Idx=setdiff(1:size(GoodEpochs,2),230:481);
    
    GoodEpochs(:,Idx,:)=[];

    epochAVG(:,:,i)=mean(GoodEpochs,3);
   
%     figure;
%     plot(epochAVG');
     
end
    ALLepochAVG=mean(epochAVG,3);
    fprintf('1)done.\n');
    
%% 2) microstate estimation on concatenated ~1-2 min rest from each subject,
% estimate optimal number of clusters
    clear meg mydata
    ALLrest=[];
for i=1:N    
    fprintf('loading rest data...\n');
    load(rest_path_list{i});
    
    mydata=eeg_emptyset;
    data=permute(meg.data,[2 1 3]);
    data=reshape(data,[size(data,1),size(data,2)*size(data,3)]);
    %data=reshape(meg.data,[size(meg.data,1)*size(meg.data,3),size(meg.data,2)])';

    fprintf('selecting first 2 minutes...\n');
    data(:,(60*meg.srate+1):end)=[];
 
    fprintf('\neliminate DC...\n'); 
    data = data - repmat(mean(data,2),1,size(data,2)); 
    
    fprintf('bandpass filtering...\n');
    filtDESIGN=designfilt('bandpassfir','FilterOrder',20, 'CutoffFrequency1',1,'CutoffFrequency2',40,'SampleRate',1000);
    data=filtfilt(filtDESIGN,data);
    
    fprintf('average referencing...\n');   
    data=data-repmat(mean(data,1),size(data,1),1);
    
    fprintf('downsampling to 256Hz...\n');
    new_fs=256;
    [p,q] =rat(new_fs/meg.srate, 0.0001);
    data=resample(double(data'),p,q)';
    mydata.srate=new_fs;
    mydata.pnts=size(data,2);
    mydata.chanlocs = chl;
    mydata.data=data;clear data
    

% bad electrodes interpolation
    fprintf('electrodes interpolation...\n');
    badel=[1,setdiff(2:128,meg.goodchannels)];
    fulldata=zeros(128,size(mydata.data,2));
    fulldata(meg.goodchannels(2:end),:)=mydata.data;
    mydata.data=fulldata; clear fulldata
    
    eeg_checkset(mydata);
    mydataC= myinterp(mydata,badel,1);
    
    ALLrest=[ALLrest,mydataC.data];
    
end


%microstate analysis
    mydata=eeg_emptyset;
    mydata.data=ALLrest;
    mydata.srate=new_fs;
    mydata.pnts=size(ALLrest,2);
    mydata=eeg_checkset(mydata);
    
    rest_GFP=sqrt(sum((mydata.data-repmat(mean(mydata.data,1),size(mydata.data,1),1)).^2)./(size(mydata.data,1)-1));
    
    ClustPar=struct('MinClasses', 3, 'MaxClasses', 10, 'GFPPeaks', 1, 'IgnorePolarity', 1, 'MaxMaps', inf, 'Restarts', 10, 'UseAAHC', 1);
    msEEG_rest=pop_FindMSTemplates(mydata,ClustPar,0,0);
    
    minK=3; maxK=10;
    tgev=zeros(1,maxK-minK+1);
    mr=zeros(1,maxK-minK+1);
    for j=minK:maxK
        tgev(j-minK+1)= msEEG_rest.msinfo.MSMaps(j).ExpVar; % explained variance (an output of pop_FindMSTemplates, saved in the output structure)
        cr=triu(abs(corr(msEEG_rest.msinfo.MSMaps(j).Maps.')),1); cr(cr==0)=[]; % correlation between maps
        mr(j-minK+1)=max(cr); % max correlation
    end
    
    % find optimal by minimizing cost function
    % minimize templates correlation:
    x=mr;
    % normalize x axis
    x=x-min(x); x=x./max(x);
    
    y=tgev;
    % normalize y axis
    y=y-min(y); y=y./max(y);
    % where plot [x y] are closest to [0 1] value:
    objfun=(x).^2+(1-y).^2;
    spt=find(objfun == min(objfun),1);
    
    figure;
    plot(x,y,'or',x,y,':b','linewidth',2); grid on;
    axis tight;
    h=vline(x(spt),':k');
    set(h,'linewidth',3);
    
    optK_rest=minK+spt-1;
    
    ep_rest= double(msEEG_rest.msinfo.MSMaps(optK_rest).Maps); % optimal number of microstates
    
    
    figure('units','normalized','outerposition',[0 0 1 0.5]);
    for l=1:optK_rest
        subplot(1,optK_rest,l); topoplot(ep_rest(l,:).',chl,'electrodes','off','emarker',{'.','k',32,1},'numcontour',6,'colormap',bipolarCM,'conv','on'); title(['M' num2str(i)]);
    end

    fprintf('2)done.\n');
%% 3) microstate estimation on grand average from visual task, with optimal
% estimate
    clear meg mydata
    
    mydata=eeg_emptyset;
    mydata.data=ALLepochAVG;
    mydata.srate=new_fs;
    mydata.pnts=size(ALLepochAVG,2);
    mydata=eeg_checkset(mydata);
    
    GFP=sqrt(sum((mydata.data-repmat(mean(mydata.data,1),size(mydata.data,1),1)).^2)./(size(mydata.data,1)-1));
    
    ClustPar=struct('MinClasses', 3, 'MaxClasses', 10, 'GFPPeaks', 1, 'IgnorePolarity', 0, 'MaxMaps', inf, 'Restarts', 10, 'UseAAHC', 1);
    msEEG_task=pop_FindMSTemplates(mydata,ClustPar,0,0);
    
    minK=3; maxK=10;
    tgev=zeros(1,maxK-minK+1);
    mr=zeros(1,maxK-minK+1);
    for j=minK:maxK
        tgev(j-minK+1)= msEEG_task.msinfo.MSMaps(j).ExpVar; % explained variance (an output of pop_FindMSTemplates, saved in the output structure)
        cr=triu(abs(corr(msEEG_task.msinfo.MSMaps(j).Maps.')),1); cr(cr==0)=[]; % correlation between maps
        mr(j-minK+1)=max(cr); % max correlation
    end
    
    % find optimal by minimizing cost function
    % minimize templates correlation:
    x=mr;
    % normalize x axis
    x=x-min(x); x=x./max(x);
    
    y=tgev;
    % normalize y axis
    y=y-min(y); y=y./max(y);
    % where plot [x y] are closest to [0 1] value:
    objfun=(x).^2+(1-y).^2;
    spt=find(objfun == min(objfun),1);
    
    figure;
    plot(x,y,'or',x,y,':b','linewidth',2); grid on;
    axis tight;
    h=vline(x(spt),':k');
    set(h,'linewidth',3);
    
    optK_task=minK+spt-1;
    
    ep_task= double(msEEG_task.msinfo.MSMaps(optK_task).Maps); % optimal number of microstates
    
    
    figure('units','normalized','outerposition',[0 0 1 0.5]);
    for l=1:optK_task
        subplot(1,optK_task,l); topoplot(ep_task(l,:).',chl,'electrodes','off','emarker',{'.','k',32,1},'numcontour',6,'colormap',bipolarCM,'conv','on'); title(['M' num2str(i)]);
    end
    fprintf('3)done.\n');
 %% 4) pair rest/task microstates based on max correlation (if above 0.7)
    r=corr(ep_rest',ep_task');
    
    %[restIDX,taskIDX]=find(abs(r)>0.7);
    newr=abs(r.*(abs(r)>0.7));
    [val,restIDXtmp]=max(newr,[],1);
    restIDX=restIDXtmp(find(val>0));
    taskIDX=find(val>0);
    
    
    
 %% 5) backfit grand average and plot:
% figure with 3 rows: top row butterfly plot of grand average
% middle row: grand average GFP with colored areas for the labels of
% microstates that have pairs in the rest analysis (point 4)
% bottom row: the microstates topographies from point 4.    

    figure;
% plot of grand average
    subplot(3,2,[1 2]);plot(ALLepochAVG');
    axis([1 size(ALLepochAVG,2) -10 10]);
    
% grand average GFP with colored areas for the labels of
% microstates that have pairs in the rest analysis
    
% using 'brute' max correlation:
r=corr(msEEG_task.data,ep_task(taskIDX,:).');
r=abs(r); % ignore polarity
THR=0.8;
[m,l]=max(r,[],2);
l(m<THR)=0;

labels=zeros(size(taskIDX,2),msEEG_task.pnts);
for ii=1:size(taskIDX,2)
   labels(ii,l==ii)=GFP(l==ii);
end
cm=colormap('jet'); cm2=cm(1:floor(64/optK_task):64,:);
T=size(GFP,2); % num time frames to display
timeRange=1:T;
timeTicks=0:25:T;
timeTicksLabels=timeTicks/msEEG_task.srate;

subplot(3,2,[3 4]);
for ii=1:size(taskIDX,2)
    tmp=labels(ii,timeRange); tmp(tmp==0)=nan; 
    h=area(tmp.','linestyle','none'); hold on; h.FaceColor=cm2(ii,:);
end
    plot(GFP,'k','linewidth',3); 
    axis([1 size(GFP,2) 0 5]);

%the microstates topographies from point 4
    for i=1:size(taskIDX,2)
        subplot(3,2,4+i);topoplot(ep_task(taskIDX(i),:).',chl,'electrodes','off','emarker',{'.','k',32,1},'numcontour',4,'conv','on');
        colormap(gca,bipolarCM);
    end
 