function [  ] = getBestSixChannelsAndCorrelateWithInverseZscore( allPatientsMeg_struct, COMPS, good_pfdr, FMA, inverse_correlation_Zscore, origine_chanloc_file_path )
%GETBESTSIXCHANNELSANDCORRELATEWITHINVERSEZSCORE Summary of this function goes here
%   Meilleur paires actuelles
%   Channel side:   ipsi        contra  transcallosal
%   good_pfdr :     7416        3592    3836
%   channel index:  90-119      33-57   35-114
%   channel label:  C26-D23     B1-B25  B3-D18

% Optimize data: aim find 6 electrodes that can correlate to the actual : data interpolated, ref avg and swap. 
% 1 pair in same hemisphere that correlate positively
% 2 pairs that correlate negatively : one pair contralesional side and 1 pair
% transcallosal

% Option 1 
% 1) Get all ipsilesional pairs (1)| contralesional pairs (2) | transcallosal
% pairs (3)
% 
% 2) Randomly pick one ipsilesional pair (1)| contralesion pair (2)
% 3) Find the best transcallosal pair (3) for highest p value of the
% correlation_result -> transcallosal_pair_optimal
% 
% 4) Randomly pick one ipsilesional pairs (1)| keep
% transcallosal_pair_optimal |
% 5) Find the best contralesional pair (2) for highest p value of the
% correlation_result -> contralesional_pair_optimal
% 
% 6) keep contralesional_pair_optimal | keep transcallosal_pair_optimal
% 7) Find the best ipsilesional pair (1) for highest p value of the
% correlation_result -> contralesional_pair_optimal
%
% 8) Start again at 2) (10 times)


% Option 2
% 1) Get all ipsilesional pairs (1)| contralesional pairs (2) | transcallosal
% pairs (3)
% 2) Find the best transcallosal pair (3) for highest p value of the
% correlation_result with the actual transcallosal pair -> transcallosal_pair_optimal 
% 3) Find the best contralesional pair (3) for highest p value of the
% correlation_result with the actual transcallosal pair -> transcallosal_pair_optimal 
% 4) Find the best ipsilesional pair (3) for highest p value of the
% correlation_result with the actual transcallosal pair -> transcallosal_pair_optimal 


% Option 3

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
pair_COMPS = COMPS(:, 1:2);

%% Option 0 : pair_indexes are the good_pfdr
% Get IC_6channels_matrix and pseudo z_score_matrix
getBestSixChannels_option0(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path)

%% Option 1 : from a random pair_index then on multiple iterations until find the optimum (condition: correlation result and p)
selected_channels_struct = getBestSixChannels_option1(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path);
run('app_connectivite1\selected_channels.m');

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
%% Option 2 : pair_indexes matrix on multiple iterations until find the optimum
selected_channels_struct = getBestSixChannels_option2(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path);
run('app_connectivite1\selected_channels.m');

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
%% Option 3 : from a random pair_index then on multiple iterations until find the optimum (condition: b-value of linear model)
selected_channels_struct = getBestSixChannels_option3(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path);
% Visualing regr
run('app_connectivite3\selected_channels.m');

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
%% Option 4 : pair_indexes on multiple iterations until find the optimum (not finished)
selected_channels_struct = getBestSixChannels_option4(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path);
run('app_connectivite1\selected_channels.m');

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   



end