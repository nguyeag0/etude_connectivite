function [good_pfdr] = getBestCorrelationWithEEGAndInverseZscore( allPatientsMeg_struct, COMPS, inverse_correlation_Zscore, channels_location, channel_location_filename )
%GETBESTCORRELATIONWITHEEGANDINVERSEZSCORE Correlation between z-score
% computed from filtered EEG with z-score computed from inverse solution
%   Detailed explanation goes here

pair_COMPS = COMPS(:, 1:2);

%% Getting imaginary coherence matrix, imaginary coherence zScore Matrix, node degree, zScore of node degree

% COMPS x patients number matrix (8128 pairs x 10 patients)
[imaginaryCoh_zScore_matrix,ICmatrix] = get_zScore_n_imagCoh_allPatients_matrix(allPatientsMeg_struct, pair_COMPS);

% Imaginary coherence matrix conversion : 8128 channel combinations x 10
% patients --> 128 channels x 128 channels x 10 subjects
ICM = convertMatrix_compByPatients2channelsByChannelsByPatients(ICmatrix, pair_COMPS);

% Node degree: mean coherence of each channels regarding all the others
[nodeDegree_zScore, node_degree] = get_zScore_n_nodeDegree_allPatients_matrix(ICM);


%% Correlation zscore node degree  : node degree on regarde chaque �lectrode avec toutes les autres
% r : rho in [-1, 1], mesure de d�pendance statistique non param�trique entre deux (Spearman)
r = corr(nodeDegree_zScore',inverse_correlation_Zscore,'type','spearman');
nanindex = find(isnan(r));
r(nanindex) = 0;
figure; topoplot(r,channels_location,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title('node degree and ic first zscore correlation');

%plot the coherence with a line
% display_structure = struct();
% display_structure.chanPairs = pair_COMPS;
% display_structure.connectStrength = mean(imaginaryCoh_zScore_matrix, 2);
% display_structure.connectStrengthLimits = [0.6 1];
% topoplot_connect(display_structure, origine_chanloc_file)

% [rs,is]=sort(r,'descend'); 
% label = allPatientsMeg_struct(1).megdata.sensor_labels;
% % label : pour afficher les canaux qui correlent le plus en ordre de
% % meilleur correlation -> mauvais : connectivit� en source
% label(is)

% Correlation zscore node degree and FMA
FMA = [23 17 22 17 46 15 18 19 54 29]'; % Fugl Meyer en pre pour chaque sujet
% rfma = corr(nodeDegree_zScore',FMA,'type','spearman');
% figure; topoplot(rfma,channels_location,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title('node degree and FMA');
% [rsfma,isfma] = sort(r,'descend');
% % label : pour afficher les canaux qui correlent le plus en ordre de meilleur correlation -> mauvais : FMA
% label(isfma)

%% On regarde la corr�lation pairwise, correlation de chaque "trait"/pair si �a corr�le avec la source
% Corr�lation du Z-score nouvellement filtr�s P et de la solution inverse
% r = corr(P',ic_first_rest_preZ_m_target_vx_10p,'type','spearman','rows','pairwise');
% Corr�lation du Z-score par paire d'�lectrode du [sujet01-10]
[correlation_result,p] = corr(imaginaryCoh_zScore_matrix',inverse_correlation_Zscore,'type','spearman','rows','pairwise');
[correlation_result,p] = corr(ICmatrix',inverse_correlation_Zscore,'type','spearman','rows','pairwise');

[beta,~,pval,in]=stepwisefit(ICmatrix',inverse_correlation_Zscore);

% Still needed with interpolated electrodes?
% nnan=sum(isnan(zScore_matrix),2);
% correlation_resultr(nnan>5) = NaN;

pairedChannels_correlation_matrix = nan(128,128);
pairedChannels_correlation_p = nan(128,128);
for k=1:8128
    pairedChannels_correlation_matrix(pair_COMPS(k,1),pair_COMPS(k,2)) = correlation_result(k);
    pairedChannels_correlation_p(pair_COMPS(k,1),pair_COMPS(k,2)) = p(k);
end
figure;imagesc(pairedChannels_correlation_matrix);colorbar;

%% Get the corrected p-values of the correlation_result and plot 
%  FDR: False Discovery Rate ; Bonferroni correction https://en.wikipedia.org/wiki/False_discovery_rate
% [pfdr,cutoff] = nut_FDR(p1,0.05);% FDR ici et non pas Bonferroni
% good_pfdr = find(pfdr<0.05)

good_pfdr = find(p<0.01)
% Plot the good pairs with p corrected values pfdr with a line
% good_pairs = pair_COMPS(good_pfdr,:);
good_pairs = pair_COMPS(good_pfdr,:);
values = correlation_result(good_pfdr);
good_pairs = pair_COMPS(in,:);
%values = correlation_result1(in);
values = r;
% good_pairs = pair_COMPS;
% values = correlation_result;
display_structure = struct();
display_structure.chanPairs = good_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;topoplot_connect(display_structure, channel_location_filename); title('Good pfdr pairs');


% IC_pseudo_Z = (ICmatrix(good_pfdr(3),:)-mean(ICmatrix(good_pfdr(1:2),:)))';  % remplace le z-score


%% Get the best and worst 10 pairs of electrode (correl bien positivement et n�gativement)
% Still need to write the test

% Positively correlating
[best_positive_channels_pairs, best_correlation_pairs] = get_best_positive_channels(pairedChannels_correlation_matrix);
best_pairs = best_correlation_pairs(:, 2:3);
values = best_correlation_pairs(:, 1);
display_structure = struct();
display_structure.chanPairs = best_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;topoplot_connect(display_structure, origine_chanloc_file); title('Best positive pairs');

% Negatively correlating
[best_negative_channels_pairs, best_correlation_pairs] = get_best_negative_channels(pairedChannels_correlation_matrix);

% plot the coherence with a line
best_pairs_neg = best_correlation_pairs(:, 2:3);
values_neg = best_correlation_pairs(:, 1);
display_structure = struct();
display_structure.chanPairs = best_pairs_neg;
display_structure.connectStrength = values_neg ;
display_structure.connectStrengthLimits = [-1 1];
figure;topoplot_connect(display_structure, origine_chanloc_file); title('best negative pairs');


end

