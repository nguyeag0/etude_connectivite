function interpolated_meg = interpolate_missing_channels(original_meg, subject_index, origine_chanloc_file_path)

    interpolated_meg = original_meg; 
    eeglab_structure = convertStruct_megToEeglab( original_meg, origine_chanloc_file_path );
    bad_channels = getBadChannels( original_meg );
    interpolated_eeglab_struct = electrodes_interpolation( original_meg, eeglab_structure, bad_channels, 1 ,subject_index);
    interpolated_meg.interpolated_data = convertEeg_eeglabToMeg(interpolated_eeglab_struct.data, original_meg.srate, size(original_meg.data, 3) );
    interpolated_meg.filtered_rawdata_channels = interpolated_meg.goodchannels;
    interpolated_meg.goodchannels = (1:1:length(interpolated_meg.sensor_labels))';
end

