clear;
username = 'dunn'; %dunn or nguyendu
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);
data_dir = ('FC_patients_data\'); % 'FC_patients_data\' ou 'etude2_data\'

% Load COMPS.mat all channel-pairs combinations
comps_path = 'data\COMPS';
channel_location_filename = 'biosemi128.xyz';
COMPS = get_COMPS_info(comps_path, channel_location_filename);

% Load biosemi eeg location file
channels_location = readlocs(channel_location_filename,'filetype','custom','format',{'-Y','X','Z','labels'},'skiplines',1);
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', channel_location_filename);

%% FC_patients_data: donn�es d'Anais : Interpolate 1, Reref avg, Swap 1
% Load patients data in a meta structure
allPatientsMeg_struct = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path);

% Load inverse correlation Z-score matrix
inverse_correlation_Zscore_file = 'ic_first_rest_preZ_m_target_vx_10p.mat';
load([current_dir 'FC_patients_data\' inverse_correlation_Zscore_file]);
load('V:\data\FC_patients_data\ic_first_rest_preZ_m_target_vx_10p.mat');
inverse_correlation_Zscore = ic_first_rest_preZ_m_target_vx_10p;
clear ic_first_rest_preZ_m_target_vx_10p inverse_correlation_Zscore_file;

% Load Fugl Meyer pre for each subject
FMA = [23 17 22 17 46 15 18 19 54 29]'; 

%% etude2_data: donn�es d'Adrian, pas besoin de swap les donn�es EEG, Interpolate 1, Reref avg, Swap 0
% Load patients data in a meta structure
load([current_dir 'etude2_data\' 'allPatientsMeg.mat'])
allPatientsMeg_struct = getPatientsStructData(current_dir, data_dir);
clear allPatientsMeg;

% Load inverse correlation Z-score array
inverse_correlation_Zscore_file = 'ic_Z_IPL_R.mat';
load([current_dir 'etude2_data\' inverse_correlation_Zscore_file]);
inverse_correlation_Zscore = V(:,1);
clear V inverse_correlation_Zscore_file;

% Load heminegligence score
heminegligence_score_file = 'CNS_T2.mat';
load([current_dir 'etude2_data\' heminegligence_score_file]);
mean_heminegligence_score = CNS_T2;
clear B C S neg2 ti CNS_T2 heminegligence_score_file;

%% Strategy 1
% Task 1
good_pfdr = getBestCorrelationWithEEGAndInverseZscore( allPatientsMeg_struct, COMPS, inverse_correlation_Zscore, channels_location, channel_location_filename);

% Task 2
%% FC_patients_data: donn�es d'Anais : Interpolate 1, Reref no, Swap 1
getBestSixChannelsAndCorrelateWithInverseZscore( allPatientsMeg_struct, COMPS, good_pfdr, FMA );

%% Strategy 2
workOnSubjectnPatientData_pca();
workOnSubjectnPatientData_fastica();
workOnSubjectnPatientData_WNDpca();
workOnSubjectnPatientData_WNDfastica();
