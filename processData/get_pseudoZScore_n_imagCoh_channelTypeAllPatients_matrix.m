function [IC_3pairs_matrix, pseudo_Zscore_matrix] = get_pseudoZScore_n_imagCoh_channelTypeAllPatients_matrix(allPatientsMeg_struct, COMPS, typeChannelsMatrix)
%GET_PSEUDOZSCORE_N_IMAGCOH_CHANNELTYPEALLPATIENTS_MATRIX Summary of this function goes here
%   Detailed explanation goes here

% typeChannelsMatrix = search_trans_pairs_matrix;%tmp

allConsideredPairsIndex = reshape(typeChannelsMatrix', [size(typeChannelsMatrix, 1)*size(typeChannelsMatrix, 2), 1]);
allConsideredPairs = [COMPS(allConsideredPairsIndex,1) COMPS(allConsideredPairsIndex,2)];

IC_3pairs_matrix = nan(length(allConsideredPairsIndex),length(allPatientsMeg_struct));
pseudo_Zscore_matrix = nan(length(typeChannelsMatrix),length(allPatientsMeg_struct));

% data = allPatientsMeg_struct(1).megdata.data;
% SC = fcm_sensorccohere31(512, data, allConsideredPairs, 'b', [8 12]);
% IC_3pairs = abs(imag(SC.coh));
% IC_3pairs_reshape = [IC_3pairs(1:3:end) IC_3pairs(2:3:end) IC_3pairs(3:3:end)]; % each line corresponds to the pairs of 3 [ipsi contra trans]
% pseudo_Zscore_matrix = IC_3pairs_reshape(:,1) - mean(IC_3pairs_reshape(:,2:3), 2);
% IC_3pairs_matrix(:,index) = IC_3pairs;
% pseudo_Zscore_matrix(:,index) = pseudo_Zscore_matrix;

for index=1:length(allPatientsMeg_struct)
    data = allPatientsMeg_struct(index).megdata.data;
    SC = fcm_sensorccohere31(512,data,allConsideredPairs, 'b',[8 12]);
    IC_3pairs = abs(imag(SC.coh));
    IC_3pairs_reshape = [IC_3pairs(1:3:end) IC_3pairs(2:3:end) IC_3pairs(3:3:end)]; % each line corresponds to the pairs of 3 [ipsi contra trans]
    IC_3pairs_matrix(:,index) = IC_3pairs;
    pseudo_Zscore_matrix(:,index) = IC_3pairs_reshape(:,1) - mean(IC_3pairs_reshape(:,2:3), 2);
end

end

%% backup code pre-vacation
% typeChannelsMatrix = search_trans_pairs_matrix;%tmp
% 
% allConsideredPairsIndex = reshape(typeChannelsMatrix', [size(typeChannelsMatrix, 1)*size(typeChannelsMatrix, 2), 1]);
% allConsideredPairs = [COMPS(allConsideredPairsIndex,1) COMPS(allConsideredPairsIndex,2)];
% 
% IC_3pairs_matrix = nan(length(allConsideredPairsIndex),length(allPatientsMeg_struct));
% pseudo_Zscore_matrix = nan(1,length(allPatientsMeg_struct));
% 
% data = allPatientsMeg_struct(1).megdata.data;
% SC = fcm_sensorccohere31(512, data, allConsideredPairs, 'b', [8 12]);
% IC_3pairs = abs(imag(SC.coh));
% IC_3pairs_reshape = [IC_3pairs(1:3:end) IC_3pairs(2:3:end) IC_3pairs(3:3:end)]; % each line corresponds to the pairs of 3 [ipsi contra trans]
% pseudo_Zscore_matrix = IC_3pairs_reshape(:,1) - mean(IC_3pairs_reshape(:,2:3), 2);
% 
% IC_3pairs_matrix(:,index) = IC_3pairs;
% pseudo_Zscore_matrix(:,index) = IC_3pairs(1) - mean(IC_3pairs(2:3));
% 
% 
% 
% for index=1:length(allPatientsMeg_struct)
%     data = allPatientsMeg_struct(index).megdata.data;
%     SC = fcm_sensorccohere31(512,data,[COMPS(pair_indexes(1),1) COMPS(pair_indexes(1),2);...
%                                    COMPS(pair_indexes(2),1) COMPS(pair_indexes(2),2);...
%                                    COMPS(pair_indexes(3),1) COMPS(pair_indexes(3),2)],...
%                                     'b',[8 12]);
%     IC_3pairs = abs(imag(SC.coh));
%     IC_3pairs_matrix(:,index) = IC_3pairs;
%     pseudo_Zscore_matrix(:,index) = IC_3pairs(3) - mean(IC_3pairs(1:2));
% end

