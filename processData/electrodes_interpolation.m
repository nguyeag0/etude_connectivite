function interpolated_eeglab_struct = electrodes_interpolation(meg, eeglab_struct, bad_channels, fast_spherical, subject_index )
%ELECTRODES_INTERPOLATION Summary of this function goes here
%   Detailed explanation goes here
% For other interpolation methodes
% https://github.com/lucklab/erplab_selectiveEegInterpolation.m/blob/master/eeg_interp.m
% http://mne-tools.github.io/dev/manual/channel_interpolation.html
% original_eeglab_data = convertEeg_megToEeglab(meg.data);
% data_points = size(meg.data,1)*size(meg.data,3);
% goodchans = meg.goodchannels;


%     ORIEEG.pnts = size(ORIEEG.data,2);

    cprintf('blue', 'Interpolating %d electrodes.\n', length(bad_channels));
    
    EEG = eeglab_struct;
%     goodchans = setdiff(1:size(EEG.data,1),bad_channels);
    goodchans = meg.goodchannels;
    
    tmpgoodlocs = EEG.chanlocs(goodchans);
    tmpbadlocs = EEG.chanlocs(bad_channels);

    xgood = [tmpgoodlocs.X];
    ygood = [tmpgoodlocs.Y];
    zgood = [tmpgoodlocs.Z];
    %         DT = DelaunayTri(xgood',ygood');
    %         figure; triplot(DT1,'r'); hold on; plot3(x1,y1,z1,'.k'); triplot(DT,'b'); plot3(xgood, ygood,zgood, '.m'); plot3( x1(1), y1(1), z1(1), '*g'); plot3( xgood(1), ygood(1), zgood(1), '*c');

    xbad = [tmpbadlocs.X];
    ybad = [tmpbadlocs.Y];
    zbad = [tmpbadlocs.Z];

    if fast_spherical
        % get theta, rad of electrodes
        % ----------------------------
        xelec = [ tmpgoodlocs.X ];
        yelec = [ tmpgoodlocs.Y ];
        zelec = [ tmpgoodlocs.Z ];
        rad = sqrt(xelec.^2+yelec.^2+zelec.^2);
        xelec = xelec./rad;
        yelec = yelec./rad;
        zelec = zelec./rad;
        rad = sqrt(xbad.^2+ybad.^2+zbad.^2);
        xbad = xbad./rad;
        ybad = ybad./rad;
        zbad = zbad./rad;

        [~, ~, ~, badchansdata] = spheric_spline( xelec, yelec, zelec, xbad, ybad, zbad, EEG.data);
        % create a temporary variable to store the data up to 128 electrodes
        data = zeros(size(meg.sensor_labels, 1), EEG.pnts);
        data(meg.goodchannels,:) = EEG.data;
        
        % Plot eeg before interpolation
%         figure_title = ['ORIGINAL', ' ', num2str(subject_index)];
%         figure; topoplot(data(:,fix(EEG.pnts/2)),EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);

%         figure; topoplot(data(:,fix(EEG.pnts/2)),EEG.chanlocs);
       
        data(bad_channels,:) = badchansdata;
        EEG.data = data;
        EEG.nbchan = size(meg.sensor_labels, 1);
        EEG = eeg_checkset(EEG);

        % Plot eeg after interpolation
%         figure_title = ['INTERPOLATION', ' ', num2str(subject_index)];
%         figure; topoplot(EEG.data(:,fix(EEG.pnts/2)),EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);
%         
    else       
        % move bad elect position inward
        [th ph r] = cart2sph(xbad, ybad, zbad);
        [x1 y1 z1] = sph2cart(th, ph, r/1.08); clear th ph r;

        % figure; plot3(x1,y1,z1,'.m'); hold on; plot3(xbad,ybad,zbad, '.r');  plot3(xgood, ygood,zgood, '.k'); plot3( x1(1), y1(1), z1(1), '*g'); plot3( xbad(1), ybad(1), zbad(1), '*c');

        badchansdata = zeros(length(bad_elec),size(ORIEEG.data,2));

        for t = 1:size(ORIEEG.data,2)
            tmpdata = double(ORIEEG.data(goodchans,t));
             f = TriScatteredInterp(xgood',ygood',zgood',tmpdata,'linear');
            badchansdata(:,t) = f(x1, y1, z1);
            if ~mod(t,100), fprintf('.');
                 if ~mod(t,10000), fprintf('  %d0k\n', t/10000);
                 end
            end              
        end
         fprintf('\ndone!\n');                             

            EEG.data(bad_channels , :) = badchansdata;
            EEG = eeg_checkset(EEG);
    end

    interpolated_eeglab_struct = EEG;
  
end