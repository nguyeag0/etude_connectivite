function [] = work_on_patientdata_19ch()
%WORK_ON_PATIENTDATA_19CH Summary of this function goes here
%   Detailed explanation goes here
%% Load path and file

% USER
username = 'nguyendu'; %dunn or nguyendu

% CHANNELS LOCATION FILE
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi128.xyz');
origine_19chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi19.xyz');

% CURRENT DIR
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);

% DATA DIR
data_dir = ('D:\data\FC_patients_data\'); % 'FC_patients_data\' ou 'etude2_data\'

% ALL 128 AND 19 PAIRS COMBINATIONS
comps_path = 'data\COMPS';
load('data\pair_COMPS.mat');
load('data\COMPS19.mat');

% To be correlated with
FMA = [23 17 22 17 46 15 18 19 54 29]'; % Fugl Meyer en pre pour chaque sujet

% For patients
load('D:\data\FC_patients_data\ic_first_rest_preZ_m_target_vx_10p.mat');





%% LOAD DATA
% Get the set of patient EEG data, taking only 19 specific channels
[ ~, allPatients_megStruct_interpoledNorerefSwapright19ChAvgreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);

patient_number = length(allPatients_megStruct_interpoledNorerefSwapright19ChAvgreref);
zScore_matrix = nan(length(COMPS19),patient_number);
ICmatrix = nan(length(COMPS19), patient_number);

% Get patient imaginary coherence and z-score based on 19 channels
for index=1:patient_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allPatients_megStruct_interpoledNorerefSwapright19ChAvgreref, index);
    ICmatrix(:,index) = index_imaginary_coherence;
    zScore_matrix(:,index) = index_zScore;
end

% corr
[r,p]=corr(zScore_matrix',ic_first_rest_preZ_m_target_vx_10p,'type','spearman','tail','right');

% All pairs (171)
selected_pairs = COMPS19(:,1:2); 
values = p;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
title('171 Pairs');

% pval < 0.05
selected_pairs = COMPS19(find(p<0.05 ),1:2);    
values = p(p<0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [0 0.06];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['zscore with ic rest preZ ' newline 'pval < 0.05  | color scale [0 0.06]'];
title(plot_title); 


% plot pair 104 / 171 | Electrodes 36 119 / 128 | Electrodes 7 18 /19
selected_pairs = COMPS19(104,1:2);    
values = p(104);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['zscore with ic rest preZ ' newline 'pair 104 | p = ' num2str(p(104))];
title(plot_title); 

end

