function [ ] = work_on_subjectdata_19ch(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref)
%WORK_ON_SUBJECTDATA_19CH Summary of this function goes here
%   Detailed explanation goes here

load('data/COMPS19.mat');

load('FC_patients_data/ic_first_rest_preZ_m_target_vx_10p.mat');    

% Load subject 1, no, 0
[ ~, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct(current_dir, origine_chanloc_file_path);

zScore_matrix = nan(length(COMPS19),length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref));
ICmatrix = nan(length(COMPS19), length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref));

zScore_matrix = nan(length(pair_COMPS),length(allSubjectsMeg_struct));
ICmatrix = nan(length(pair_COMPS), length(allSubjectsMeg_struct));

% Get patient imaginary coherence
index = 1;
[ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref, index);

% stepwisefit with inverse_correlation_Zscore
[beta,~,pval,in]=stepwisefit(index_zScore',ic_first_rest_preZ_m_target_vx_10p);

end

