function [] = workOnSubjectnPatientData_pca()
%WORKONSUBJECTNPATIENTDATA Summary of this function goes here
%   Detailed explanation goes here
clear 
% USER
username = 'nguyendu'; %dunn or nguyendu

% CHANNELS LOCATION FILE
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi128.xyz');
origine_19chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi19.xyz');
egi_origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\egi204.xyz');

% CURRENT DIR
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);

% DATA DIR
data_dir = ('D:\data\Data_Anais_Leslie_Lea\'); % 'FC_patients_data\' ou 'etude2_data\' ou 'subjects_data\'
% data_dir = ('C:\data\subjects_data\');

% ALL 128 AND 19 PAIRS COMBINATIONS
comps_path = 'data\COMPS';
load('data\pair_COMPS.mat');
load('data\COMPS19.mat');

% To be correlated with
load('D:\data\Data_Anais_Leslie_Lea\NewPrecentral_values.mat'); % Precentral(:,1) --> Left  | Precentral(:,2) --> Right

% To be correlated with
  
%% LOAD DATA
% Get the set of patient EEG data, taking only 19 specific channels :
% Interpolated 1 | Re-ref 'no' | Swap 0

% Get all subject MEG data : Interpolation 1 | Reref 'no' | Swap 0
allSubjects_megStruct_interpoledNorerefNoswap = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path, 'biosemi'); % all subject raw data
allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref = struct(allSubjects_megStruct_interpoledNorerefNoswap); % subject data reduced to 19ch
subjectNumber = length(allSubjects_megStruct_interpoledNorerefNoswap);
clear allSubjects_megStruct_interpoledNorerefNoswap;

% Load the wanted channels and index (here 19)
load('data/channels19_1020_biosemi.mat');
channels19_index = channels19_1020_biosemi.index;

% Get the 19 EEG channels and reref avg on 19 channels
for i = 1:subjectNumber
%     allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i) = allSubjects_megStruct_interpoledNorerefNoswap(i);    
    data19 = allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data(:,channels19_index,:);
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data19 = data19;
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.channels19 = channels19_index;    
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data = data19;
    megdata = allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata;
    megdata = nut_eegref(megdata, 'AVG');
%    megdata = eegref_smallLaplacian( megdata, origine_19chanloc_file_path );
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata = megdata;
end

% Concatenate all subject EEG data in one, as if we had one big resting
% state of 70*300s
dataall=[];
for i = 1:subjectNumber
    
    % Downsample EEG data if meg.srate = 1024
    if allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.srate>512
        allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata = nut_downsample(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata,2);
    end
    data19 = allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data;
    data2D = reshape( permute(data19,[1 3 2]), ...
                   [size(data19, 1)*size(data19, 3) size(data19, 2) ]);     % reshape [1024*300 19] or [512*300 19]     
    dataall = cat(1,dataall,zscore(data2D));
end

% zeemg(1:512,data19)
% zeemg(1:3000,data2D(1:3000,:))
% zeemg(1:3000,zscore(data2D(1:3000,:)))

[coeff,score]=pca(dataall); 
save data/pca_coeff coeff 
load data/pca_coeff
save D:/data/4Adrian/pca_coeffScore coeff score
% figure;imagesc(coeff)

for i = 1:length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref)
    data2D = score(1:512*300,:);
    score(1:512*300,:)=[];
    data3D = reshape(data2D, size(data19, 1), size(data19, 3), size(data19, 2));
    data3D = permute(data3D,[1 3 2]);
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data = data3D;
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata.dataPCA = data3D;
end

% zeemg(1:512,data3D)

%% GET ICMATRIX zSCORE_MATRIX
subject_number = length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref);
zScore_matrix = nan(length(COMPS19),subject_number);
ICmatrix = nan(length(COMPS19), subject_number);

% Get patient imaginary coherence and z-score based on 19 channels
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref, index);
    ICmatrix(:,index) = index_imaginary_coherence;
    zScore_matrix(:,index) = index_zScore;
end
save data/pca_ICzScore_matrix_70subject ICmatrix zScore_matrix
load data/pca_ICzScore_matrix_70subject

%% STEPWISEFIT with 70 healthy subject 

% stepwisefit with inverse_correlation_Zscore Precentral area left and
% right
[beta_preLeftVal_swf,~,pval_preLeftVal_swf,in_preLeftVal_swf]=stepwisefit(zScore_matrix',Precentral(:,1));
[beta_preRightVal_swf,~,pval_preRightVal_swf,in_preRightVal_swf]=stepwisefit(zScore_matrix',Precentral(:,2));

% Get best 8 indexes of the inPair left and right
[~,idxL]=sort(pval_preLeftVal_swf);
idxL=idxL(1:8);
pval_preLeftVal_swf(idxL)
in_preLeftVal_swf(idxL)

[~,idxR]=sort(pval_preRightVal_swf);
idxR=idxR(1:8);
pval_preRightVal_swf(idxR)
in_preRightVal_swf(idxR)

% Get the betas left and right with glmfit and the best indexes from
% stepwisefit pval (Need to reduce the zScore matrix to avoid overfitting with the patients number of individual, 8 indexes for 10 patients)
[beta_preLeftVal_glm]=glmfit(zScore_matrix(idxL,:)',Precentral(:,1),'normal');
[beta_preRightVal_glm]=glmfit(zScore_matrix(idxR,:)',Precentral(:,2),'normal');

% meanbeta_preLeftVal = mean(beta_preLeftVal);
% meanbeta_preRightVal = mean(beta_preRightVal);
% 
% inPair_left = find(in_preLeftVal);
% inPair_right = find(in_preRightVal);


%% CORR

[r_left, p_left] = corr(zScore_matrix',Precentral(:,1),'type','spearman');
[r_right, p_right] = corr(zScore_matrix',Precentral(:,2),'type','spearman');

%good_p = (find(p_right<0.05))
%  FDR: False Discovery Rate
[pfdr,cutoff] = nut_FDR(p_right,0.05);
good_pfdr = find(pfdr<0.05)


%% LOAD Patients' data

% Patient d'Anais
patient_data_dir = ('D:\data\FC_patients_data\');
load('D:\data\FC_patients_data\ic_first_rest_preZ_m_target_vx_10p.mat');

% Patient d'Adrian (warning, only interpolation changed for egi system)
patient_data_dir = ('D:\data\stroke_dataset_biosemi\');
load('D:\data\stroke_dataset\ic_Precentral_LR.mat');

% egi = 1:5;
biosemi1 = 6:21;
% brainvision = 22:28;
biosemi2 = 29:35;


%% Interpolate 1, Reref avg, Swap 1
% Load patients data in a meta structure: Interpolate 1 | Re-ref 'no' |
% Swap '0'
% Donn�es Anais
% allPatientsMeg_struct = getPatientsStructData(current_dir, patient_data_dir, origine_chanloc_file_path, 'biosemi');
% allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref = allPatientsMeg_struct;

% Donn�es Adrian
allPatientsMeg_struct = getPatientsStructData(current_dir, patient_data_dir, origine_chanloc_file_path, 'biosemi');
ic_Precentral_LR_biosemi = V([biosemi1 biosemi2],:);
% allPatientsMeg_struct_biosemi = allPatientsMeg_struct([biosemi1 biosemi2]);
allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref = allPatientsMeg_struct;
clear allPatientsMeg_struct_biosemi allPatientsMeg_struct;
patientsNum = length(allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref);

% Get the 19 EEG channels and reref avg on 19 channels
for i = 1:patientsNum
%     allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i) = allSubjects_megStruct_interpoledNorerefNoswap(i);    
    cprintf('blue', 'Reduce to 19 channels patient EEG #%d\n', i);
    data19 = allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data(:,channels19_index,:);
    
    allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data19 = data19;
    allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref(i).megdata.channels19 = channels19_index;    
    
    allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref(i).megdata.data = data19;
    megdata = allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref(i).megdata;
    megdata = nut_eegref(megdata, 'AVG');
%    megdata = eegref_smallLaplacian( megdata, origine_19chanloc_file_path );
    
    allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref(i).megdata = megdata;
end

dataallP = concatenateEegDataall(allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref);

PCAdataall_patient = dataallP * coeff;

allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref = dividePCAdataToAllParticipant(...
                                allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref, PCAdataall_patient, data19);


%% GET ICMATRIX zSCORE_MATRIX
patient_number = length(allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref);
patient_zScore = nan(length(COMPS19),patient_number);
patient_IC = nan(length(COMPS19), patient_number);

% Get patient imaginary coherence and z-score based on 19 channels
for index=1:patient_number
    cprintf('blue', 'Patient IC and zScore #%d\n', index);
    [ index_zScore, index_imaginary_coherence, patient_comps ] = get_index_zScore(allPatientsMeg_struct_interpoledNorerefNoswap19ChAvgreref, index);
    patient_IC(:,index) = index_imaginary_coherence;
    patient_zScore(:,index) = index_zScore;
end

save data/pca_zScore_matrix_23patients patient_IC patient_zScore
load data/pca_zScore_matrix_23patients

% patientZscore_subjectInPairLeft = patient_zScore(inPair_left,:);
% patientZscore_subjectInPairRight = patient_zScore(inPair_right,:);

%% REGRESS inPairs of best indexes on patients' zScore

% y are the predicted values for the generalized linear model, predicted
% with the betas of glmfit with subject
yL = glmval(beta_preLeftVal_glm,patient_zScore(idxL,:)','identity');
yR = glmval(beta_preRightVal_glm,patient_zScore(idxR,:)','identity');

% lesion side Anais
load data\subject_lesion_side_patient
isr=strcmp({subject_lesion_side.lesion_side},'D')';

% lesion side Adrian
% load('D:\data\stroke_dataset\side35pat.mat');
% side_biosemi = side([biosemi1 biosemi2]);
% isr = find(side_biosemi == 'R'); % index of patient with right lesion side
% isl = find(side_biosemi == 'L'); % index of patient with right lesion side

% Combine in y the yL and yR regarding the lesion side
% Donn�es Anais
% y = nan(10,1);
% y(isr)=yR(isr);
% y(~isr)=yL(~isr);
% [r,p]=corr(y,ic_first_rest_preZ_m_target_vx_10p)

% Donn�es Adrian
[rL, pL] = corr(yL, ic_Precentral_LR_biosemi(:,1));
[rR, pR] = corr(yR, ic_Precentral_LR_biosemi(:,2));

% Corr between patients interesting zscore pairs and the ic_preZ
% [betas_left,~,~,~,stats_regress_left]=regress(ic_first_rest_preZ_m_target_vx_10p,[ones(10,1) patientZscore_subjectInPairLeft' ]); %,'linear',{'adjrsquare' 'beta'}); 
% [betas_right,~,~,~,stats_regress_right]=regress(ic_first_rest_preZ_m_target_vx_10p,[ones(10,1) patientZscore_subjectInPairRight' ]); %,'linear',{'adjrsquare' 'beta'}); 
% 
% b_left = betas_left(2:end);
% b_right = betas_right(2:end);


%% CORR Patient

% Corr between patients interesting zscore pairs and the ic_preZ
% [r, p] = corr(patient_zScore',ic_first_rest_preZ_m_target_vx_10p,'type','spearman');
% patientP_atSubjectGoodPIndex = p(good_p);



end

