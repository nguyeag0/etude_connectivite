function [ ] = work_on_subjectdata_19ch(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref)
% WORK_ON_SUBJECTDATA_19CH Summary of this function goes here

%% Load path and file

% USER
username = 'nguyendu'; %dunn or nguyendu

% CHANNELS LOCATION FILE
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi128.xyz');
origine_19chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi19.xyz');

% CURRENT DIR
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);

% DATA DIR
data_dir = ('D:\data\Data_Anais_Leslie_Lea\'); % 'FC_patients_data\' ou 'etude2_data\' ou 'subjects_data\'

% ALL 128 AND 19 PAIRS COMBINATIONS
comps_path = 'data\COMPS';
load('data\pair_COMPS.mat');
load('data\COMPS19.mat');

% To be correlated with
load('D:\data\Data_Anais_Leslie_Lea\NewPrecentral_values.mat');


%% LOAD DATA
% Get the set of patient EEG data, taking only 19 specific channels :
% Interpolated 1 | Re-ref 'no' | Swap 0
[ ~, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);
% [ ~, allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref ] = get_all_subjects_megstruct(current_dir, origine_chanloc_file_path);

subject_number = length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref);
zScore_matrix = nan(length(COMPS19),subject_number);
ICmatrix = nan(length(COMPS19), subject_number);

% Get patient imaginary coherence and z-score based on 19 channels
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref, index);
    ICmatrix(:,index) = index_imaginary_coherence;
    zScore_matrix(:,index) = index_zScore;
end

% interestingPairs = find(any(ismember(COMPS19(:,1:2),[9 17]),2)); % pairs where channel 9 and 17 /19 appear
% ICmatrix = ICmatrix(interestingPairs,:);
% zScore_matrix = zScore_matrix(interestingPairs,:);

%% Corr

% corr
[rL_noswap,pL_noswap]=corr(zScore_matrix',Precentral(:,1),'type','spearman','tail','right');
[rR_noswap,pR_noswap]=corr(zScore_matrix',Precentral(:,2),'type','spearman','tail','right');

% All pairs (171)
selected_pairs = COMPS19(:,1:2); 
values = pL_noswap;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
title('171 Pairs: zscore with Precentral Left Values');

% pval < 0.05
selected_pairs = COMPS19(find(pL_noswap<0.05 ),1:2);    
values = pL_noswap(pL_noswap<0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [0 0.06];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['zscore with Precentral Left Values tail right' newline 'pval < 0.05  | color scale [0 0.06]'];
title(plot_title); 

% pL_noswap : swap to right side and compare

pL_SwappedToRight = pL_noswap;
rL_SwappedToRight = rL_noswap;
idnum = length(pL_SwappedToRight);
for i=1:idnum
    pL_SwappedToRight( COMPS19(i,9), 2) = pL_noswap(i,1);
    rL_SwappedToRight( COMPS19(i,9), 2) = rL_noswap(i,1);
end

stat_p_3D = cat(3, pL_SwappedToRight(:,2), pR_noswap);
p_binom = binomstat2('p', stat_p_3D, 0.05);
meanrr = tanh(mean(atanh([rL_SwappedToRight(:,2) rR_noswap]),2));
goodr = (p_binom<0.05);

values = meanrr(goodr);
selected_pairs = COMPS19(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 

%%
% [rr_swap,pr_swap]=corr(zScore_matrix_19ChSwap',Precentral(:,2),'type','spearman');
% [rl_swap,pl_swap]=corr(zScore_matrix_19ChSwap',Precentral(:,1),'type','spearman');

meanrr = tanh(mean(atanh([rr_swap rr_noswap]),2));
goodr=(abs(meanrr)>0.3);

values = meanrr(goodr);
selected_pairs = COMPS_ind(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 

[r,p]=corr([zScore_matrix_19ChNoSwap(goodr,:)';zScore_matrix_19ChSwap(goodr,:)'],[Precentral(:,1);Precentral(:,2)])
[beta,~,pval,in] = stepwisefit([zScore_matrix_19ChNoSwap(goodr,:)';zScore_matrix_19ChSwap(goodr,:)'],[Precentral(:,1);Precentral(:,2)]);


% plot sandbox
m1 = mean(betas_current1);
selected_pairs = COMPS_ind(:,1:2);    
values = betas_current1;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['mean betas_current1 = ' num2str(m1) newline ...
            'subjet : interpolated, no reref, NO swap, 19ch, ref avg, precentral LEFT values' newline ...
            'all beta1 with regress'];
title(plot_title);



%% STEPWISEFIT

% stepwisefit with inverse_correlation_Zscore
[beta_preLeftVal,~,pval_preLeftVal,in_preLeftVal]=stepwisefit(zScore_matrix',Precentral(:,1));
[beta_preRightVal,~,pval_preRightVal,in_preRightVal]=stepwisefit(zScore_matrix',Precentral(:,2));

meanbeta_preLeftVal = mean(beta_preLeftVal);
meanbeta_preRightVal = mean(beta_preRightVal);

load('data/channels19_1020_biosemi.mat');
xyz = xyzread(origine_chanloc_file_path, 'headerlines', 1);
channels_ind = find(ismember(xyz.labels, channels19_1020_biosemi.biosemi));
COMPS_ind = [channels_ind(COMPS19(:,1)) channels_ind(COMPS19(:,2))];

% all 171 pairs
% selected_pairs = COMPS_ind(:,1:2);    
selected_pairs = COMPS19(:,1:2);    
values = beta_preRightVal;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['171 Pairs' newline 'Beta with Precentral Right Values, mean beta = ' num2str(meanbeta_preRightVal)];
title(plot_title);   

% Pairs with beta <-0.08 or >0.08
selected_pairs = COMPS19(find(beta_preLeftVal>0.08 | beta_preLeftVal<-0.08 ),1:2);    
values = beta_preLeftVal(beta_preLeftVal>0.08 | beta_preLeftVal<-0.08 );
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['Beta with Precentral Left Values, mean beta = ' num2str(meanbeta_preLeftVal) newline ...
            'beta <-0.08 or >0.08'];
title(plot_title);

%% REGRESS

% zScore_matrix = zScore_matrix_19ChNoSwap;
[betas_regress_preLeftVal,~,~,~,stats_regress_preLeftVal]=regress(Precentral(:,1),[ones(70,1) zScore_matrix' ]); %,'linear',{'adjrsquare' 'beta'});
[betas_regress_preRightVal,~,~,~,stats_regress_preRightVal]=regress(Precentral(:,2),[ones(70,1) zScore_matrix' ]); %,'linear',{'adjrsquare' 'beta'});

meanbetaRegress_preLeftVal = mean(betas_regress_preLeftVal); %172 !!
meanbetaRegress_preRightVal = mean(betas_regress_preRightVal);%172!!
betas_regress_preLeftVal171 = betas_regress_preLeftVal(2:end); 
betas_regress_preRightVal171 = betas_regress_preRightVal(2:end);

% all 171 pairs
% selected_pairs = COMPS_ind(:,1:2);    
origine_19chanloc_file_path = 'C:\Users\nguyendu\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi19.xyz';
selected_pairs = COMPS19(:,1:2);    
values = betas_regress_preLeftVal171;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['171 Pairs' newline 'Beta with Precentral Left Values, mean beta = ' num2str(meanbetaRegress_preLeftVal)];
title(plot_title);   

% Pairs with beta <-0.2 or >0.2
selected_pairs = COMPS19(find(betas_regress_preLeftVal171>0.2| betas_regress_preLeftVal171<-0.2 ),1:2);    
values = betas_regress_preLeftVal171(betas_regress_preLeftVal171>0.2 | betas_regress_preLeftVal171<-0.2 );
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['Beta with Precentral Left Values, mean beta = ' num2str(meanbetaRegress_preLeftVal) newline ...
            'beta <-0.2 or >0.2'];
title(plot_title);



end

