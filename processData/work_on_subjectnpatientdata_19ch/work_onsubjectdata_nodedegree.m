function [outputArg1,outputArg2] = work_onsubjectdata_nodedegree(inputArg1,inputArg2)
%WORK_ONSUBJECTDATA_NODEDEGREE Summary of this function goes here
%   Detailed explanation goes here
% USER
username = 'nguyendu'; %dunn or nguyendu

% CHANNELS LOCATION FILE
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi128.xyz');
origine_19chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi19.xyz');

% CURRENT DIR
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);

% DATA DIR
data_dir = ('D:\data\Data_Anais_Leslie_Lea\'); % 'FC_patients_data\' ou 'etude2_data\' ou 'subjects_data\'
% data_dir = ('C:\data\subjects_data\');

% ALL 128 AND 19 PAIRS COMBINATIONS
comps_path = 'data\COMPS';
load('data\pair_COMPS.mat');
load('data\COMPS19.mat');

% To be correlated with
load('D:\data\Data_Anais_Leslie_Lea\NewPrecentral_values.mat'); % Precentral(:,1) --> Left  | Precentral(:,2) --> Right


%% LOAD DATA
% Get the set of patient EEG data, taking only 19 specific channels :
% Interpolated 1 | Re-ref 'no' | Swap 0

% Get all subject MEG data : Interpolation 1 | Reref 'no' | Swap 0
allSubjects_megStruct_interpoledNorerefNoswap = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path); % all subject raw data
allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref = struct(allSubjects_megStruct_interpoledNorerefNoswap); % subject data reduced to 19ch

% Load the wanted channels and index
load('data/channels19_1020_biosemi.mat');
channels19_index = channels19_1020_biosemi.index;

% Get the 19 EEG channels and reref avg on 19 channels
for i = 1:length(allSubjects_megStruct_interpoledNorerefNoswap)
%     allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i) = allSubjects_megStruct_interpoledNorerefNoswap(i);    
    data19 = allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.data(:,channels19_index,:);
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.data19 = data19;
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.channels19 = channels19_index;    
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.data = data19;
    megdata = allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata;
    megdata = nut_eegref(megdata, 'AVG');
%    megdata = eegref_smallLaplacian( megdata, origine_19chanloc_file_path );
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata = megdata;
end


%% GET ICMATRIX zSCORE_MATRIX
subject_number = length(allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref);
zScore_matrix = nan(length(COMPS19),subject_number);
ICmatrix = nan(length(COMPS19), subject_number);

% Get patient imaginary coherence and z-score based on 19 channels
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref, index);
    ICmatrix(:,index) = index_imaginary_coherence;
    zScore_matrix(:,index) = index_zScore;
end

zScore_matrix_noswap = zScore_matrix;
zScore_matrix_swap = zScore_matrix;


%% Getting imaginary coherence matrix, imaginary coherence zScore Matrix, node degree, zScore of node degree

% Imaginary coherence matrix conversion : 8128 channel combinations x 10 patients --> 128 channels x 128 channels x 10 subject
    ICM = nan(19,19,70);
    for s=1:70
        for k=1:171
            ICM(COMPS19(k,1),COMPS19(k,2),s)=ICmatrix(k,s);
            ICM(COMPS19(k,2),COMPS19(k,1),s)=ICmatrix(k,s);
        end
    end


% Node degree: mean coherence of each channels regarding all the others
  node_degree = squeeze(nanmean(ICM));

    nodeDegree_zScore=zeros(size(node_degree)); 
    for k=1:70 % 1:10 for patient of anais
        nodeDegree_zScore(:,k)=(node_degree(:,k) - nanmean(node_degree(:,k))) ./ nanstd(node_degree(:,k));
%         nodeDegree_zScore(:,k)=(node_degree(:,k) - mean(node_degree(:,k))) ./ std(node_degree(:,k));
    end


end

