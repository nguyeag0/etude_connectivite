function [ ] = work_on_subjectdata_19ch(current_dir, origine_chanloc_file_path)
%WORK_ON_SUBJECTDATA_19CH Working on subject EEG data with 19 channels
% Set of data 1:
% Data interpoled, no rereferenced, NOT swapped, reduced to 19 channels, rereferenced
% with the 19 channels
% 
% Set of data 2:
% Data interpoled, no rereferenced, swapped, reduced to 19 channels, rereferenced
% with the 19 channels
% 
% Precentral left values
% Precentral right values
% 
% Beta1 : set 1 with left values
% Beta2 : set 2 with right values
% Beta3 : set 3 with left values
% Beta4 : set 4 with right values


% [ allSubjects_megStruct_interpoledNorerefNoswap, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct(current_dir, origine_chanloc_file_path);

data_dir = ('E:\Data_Anais_Leslie_Lea\');
load('data/COMPS19.mat');
load([data_dir 'Precentral_L_values.mat']);
load([data_dir 'Precentral_R_values.mat']);   

% Interpole : 1 | Reref : 'no' | Swap : 0
[ ~, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);
% Interpole : 1 | Reref : 'no' | Swap : 1
[ ~, allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);

% Get precentral values left and right according to subject number
subject_number = length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref);
Precentral_L_values_temp = Precentral_L_values;
Precentral_R_values_temp = Precentral_R_values;

% Get patient imaginary coherence
ICmatrix_19ChNoSwap = nan(length(COMPS19), subject_number);
zScore_matrix_19ChNoSwap = nan(length(COMPS19),subject_number);
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref, index);
    ICmatrix_19ChNoSwap(:,index) = index_imaginary_coherence;
    zScore_matrix_19ChNoSwap(:,index) = index_zScore;
end

ICmatrix_19ChSwap = nan(length(COMPS19), subject_number);
zScore_matrix_19ChSwap = nan(length(COMPS19),subject_number);
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref, index);
    ICmatrix_19ChSwap(:,index) = index_imaginary_coherence;
    zScore_matrix_19ChSwap(:,index) = index_zScore;
end

% corr
[rr_noswap,pr_noswap]=corr(zScore_matrix_19ChNoSwap',Precentral_R_values,'type','spearman');
[rl_noswap,pl_noswap]=corr(zScore_matrix_19ChNoSwap',Precentral_L_values,'type','spearman');

[rr_swap,pr_swap]=corr(zScore_matrix_19ChSwap',Precentral_R_values,'type','spearman');
[rl_swap,pl_swap]=corr(zScore_matrix_19ChSwap',Precentral_L_values,'type','spearman');


meanrr = tanh(mean(atanh([rr_swap rr_noswap]),2));
goodr=(abs(meanrr)>0.3);

values = meanrr(goodr);
selected_pairs = COMPS_ind(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 

[r,p]=corr([zScore_matrix_19ChNoSwap(goodr,:)';zScore_matrix_19ChSwap(goodr,:)'],[Precentral_L_values;Precentral_R_values])
[beta,~,pval,in] = stepwisefit([zScore_matrix_19ChNoSwap(goodr,:)';zScore_matrix_19ChSwap(goodr,:)'],[Precentral_L_values;Precentral_R_values]);

% Calculate betas with stepwisefit
[beta_lNoSwap,~,pval1,in1] = stepwisefit(zScore_matrix_19ChNoSwap',Precentral_L_values_temp);
[beta_rNoSwap,~,pval2,in2] = stepwisefit(zScore_matrix_19ChNoSwap',Precentral_R_values_temp);
[beta3,~,pval3,in3] = stepwisefit(zScore_matrix_19ChSwap',Precentral_L_values_temp);
[beta4,~,pval4,in4] = stepwisefit(zScore_matrix_19ChSwap',Precentral_R_values_temp);

mean_betas_lNoSwap = mean(beta_lNoSwap);
mean_betas_rNoSwap = mean(beta_rNoSwap);
mean_betas3 = mean(beta3);
mean_betas4 = mean(beta4);

%% Topoplot the 171 pairs with betas as values
origine_chanloc_file = 'biosemi128.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);
xyz = xyzread(origine_chanloc_file_path, 'headerlines', 1);
load('data/channels19_1020_biosemi.mat');
channels_ind = find(ismember(xyz.labels, channels19_1020_biosemi.biosemi));
COMPS_ind = [channels_ind(COMPS19(:,1)) channels_ind(COMPS19(:,2))];

% All 171 pairs
selected_pairs = COMPS_ind(:,1:2);    
values = beta_lNoSwap;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['mean beta left NoSwap = ' num2str(mean_betas_lNoSwap) newline ...
            'subjet : interpolated, no reref, SWAP, 19ch, ref avg, precentral LEFT values'];
title(plot_title);


% Pairs with beta <-0.3 or >0.3
selected_pairs = COMPS_ind(find(beta_lNoSwap>0.2 | beta_lNoSwap<-0.2 ),1:2);    
values = beta_lNoSwap(beta_lNoSwap>0.3 | beta_lNoSwap<-0.3 );
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['mean betas left NoSwap = ' num2str(mean_betas_lNoSwap) newline ...
            'subjet : interpolated, no reref, NO swap, 19ch, ref avg, precentral LEFT values, beta <-0.3 or >0.3'];
title(plot_title);

% Pairs with beta <-0.3 or >0.3
selected_pairs = COMPS_ind(find(beta2<-0.15 | beta2>0.15),1:2);    
values = beta2(beta2<-0.15 | beta2>0.15);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['mean betas2 = ' num2str(mean_betas2) newline ...
            'subjet : interpolated, no reref, NO swap, 19ch, ref avg, precentral RIGHT values, beta <-0.3 or >0.3' newline ...
            'beta2<-0.15 | beta2>0.15'];
title(plot_title);



%% reref slap: no swap
% Interpole : 1 | Reref : 'no' | Swap : 0
origine_chanloc_file = 'biosemi128.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);

[ ~, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);

origine_chanloc_file = 'biosemi19.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);

subject_num = length(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref);
for i=1:subject_num
    meg =  allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata;   
    megslap = eegref_smallLaplacian(meg, origine_chanloc_file_path);
    allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref(i).megdata = megslap;
end
allSubjects_megStruct_interpoledNorerefNoswap19ChSlapref = allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref;
clear allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref

ICmatrix_19ChNoSwap = nan(length(COMPS19), subject_number);
zScore_matrix_19ChNoSwap = nan(length(COMPS19),subject_number);
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChSlapref, index);
    ICmatrix_19ChNoSwap(:,index) = index_imaginary_coherence;
    zScore_matrix_19ChNoSwap(:,index) = index_zScore;
end

[rr_noswap,pr_noswap]=corr(zScore_matrix_19ChNoSwap',Precentral_R_values,'type','spearman');
[rl_noswap,pl_noswap]=corr(zScore_matrix_19ChNoSwap',Precentral_L_values,'type','spearman');

selected_pairs = COMPS19(pr_noswap < 0.05,1:2);    
values = rr_noswap(pr_noswap < 0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['pr noswap <0.05'];
title(plot_title);


selected_pairs = COMPS19(pl_noswap < 0.05,1:2);    
values = rl_noswap(pl_noswap < 0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['pl noswap <0.05'];
title(plot_title);

end

%%%% reref slap: swap
% Interpole : 1 | Reref : 'no' | Swap : 1
origine_chanloc_file = 'biosemi128.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);

[ ~, allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);

origine_chanloc_file = 'biosemi19.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);

subject_num = length(allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref);
for i=1:subject_num
    meg =  allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref(i).megdata;   
    megslap = eegref_smallLaplacian(meg, origine_chanloc_file_path);
    allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref(i).megdata = megslap;
end
allSubjects_megStruct_interpoledNorerefSwap19ChSlapref = allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref;
clear allSubjects_megStruct_interpoledNorerefSwap19ChAvgreref

ICmatrix_19ChSwap = nan(length(COMPS19), subject_number);
zScore_matrix_19ChSwap = nan(length(COMPS19),subject_number);
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefSwap19ChSlapref, index);
    ICmatrix_19ChSwap(:,index) = index_imaginary_coherence;
    zScore_matrix_19ChSwap(:,index) = index_zScore;
end

[rr_swap,pr_swap]=corr(zScore_matrix_19ChSwap',Precentral_R_values,'type','spearman');
[rl_swap,pl_swap]=corr(zScore_matrix_19ChSwap',Precentral_L_values,'type','spearman');

selected_pairs = COMPS19(pr_swap < 0.05,1:2);    
values = rr_swap(pr_swap < 0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['pr swap <0.05'];
title(plot_title);


selected_pairs = COMPS19(pl_swap < 0.05,1:2);    
values = rl_swap(pl_swap < 0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 
plot_title = ['pl swap <0.05'];
title(plot_title);

origine_chanloc_file = 'biosemi19.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);

mean_ls_rn = tanh(mean(atanh([rl_swap rr_noswap]),2));
goodr=(abs(mean_ls_rn)>0.3);

values = mean_ls_rn(goodr);
selected_pairs = COMPS19(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path); 

mean_rs_ln = tanh(mean(atanh([rr_swap rl_noswap]),2));
goodr=(abs(mean_rs_ln)>0.3);

values = mean_rs_ln(goodr);
selected_pairs = COMPS19(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file_path);


[r,p]=corr([zScore_matrix_19ChNoSwap(goodr,:)';zScore_matrix_19ChSwap(goodr,:)'],[Precentral_L_values;Precentral_R_values])
[beta,~,pval,in] = stepwisefit([zScore_matrix_19ChNoSwap(goodr,:)';zScore_matrix_19ChSwap(goodr,:)'],[Precentral_L_values;Precentral_R_values]);

