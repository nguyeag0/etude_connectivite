function [ allPatientsMeg_struct ] = rereference_all_megdata( allPatientsMeg_struct, newConsideredReference )
%REREFERENCE_ALL_MEGDATA re-reference all data regarding chosen new
%reference
%   allPatientsMeg_struct : all meg structure of all subject in one meta
%   structure
%   newConsideredReference = 'AVG' / 'Cz'

    sensor_labels = allPatientsMeg_struct(1).megdata.sensor_labels;
    for index=1:size(allPatientsMeg_struct, 2)
        
        if(strcmp(newConsideredReference, 'avg') || ~isempty(find(strcmp(sensor_labels, newConsideredReference))))
            cprintf('blue', 'Reref MEG data Patient %d with %s electrode.\n', allPatientsMeg_struct(index).patientindex, newConsideredReference);
            allPatientsMeg_struct(index).megdata = nut_eegref(allPatientsMeg_struct(index).megdata, newConsideredReference);
            allPatientsMeg_struct(index).megdata.rereference = newConsideredReference;
        elseif(strcmp(newConsideredReference, 'slap'))
            cprintf('blue', 'Reref MEG data Patient %d with small Laplacian.\n', allPatientsMeg_struct(index).patientindex);
            allPatientsMeg_struct(index).megdata.rereference = newConsideredReference;
            if(size(allPatientsMeg_struct(index).megdata.data, 2)~=128)
                error('Channels number is less than 128. Consider interpolate the data or have the complete raw set data.');
            end
            chanlocs_filename = 'biosemi128.xyz';
            allPatientsMeg_struct(index).megdata = eegref_smallLaplacian(allPatientsMeg_struct(index).megdata, chanlocs_filename);
        else
            cprintf('blue', 'Patient %d no rereferencing.\n', allPatientsMeg_struct(index).subjectindex);
        end
               
    end
    
end

