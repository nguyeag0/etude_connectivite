function [ best_negative_channels_pairs, best_correlation_pairs ] = get_best_negative_channels( pairedChannels_correlation_matrix )
%GET_BEST_negative_CHANNELS Summary of this function goes here
%   Detailed explanation goes here


    sorted_correlation_matrix=pairedChannels_correlation_matrix(:);
    sorted_correlation_matrix(isnan(sorted_correlation_matrix)) = [];
    % sorted_correlation_matrix = sort(pairedChannels_correlation_matrix(:), 'descend');
    sorted_correlation_matrix = unique(sorted_correlation_matrix);
    best_correlation_pairs = zeros(10, 3);
    counter = 1;

    if size(sorted_correlation_matrix, 1) >= 10
        for ind=1:10
            best_negative_channels_pairs(ind).best_negative_value = sorted_correlation_matrix(ind, 1);
            [rowsOfMaxes colsOfMaxes] = find(pairedChannels_correlation_matrix == sorted_correlation_matrix(ind, 1));
            best_negative_channels_pairs(ind).rows_of_maxes = rowsOfMaxes;
            best_negative_channels_pairs(ind).columns_of_maxes = colsOfMaxes;
            
            for rows_ind=1:length(rowsOfMaxes);
                if (counter > 10)
                    break
                end
                best_correlation_pairs(counter, :, :) = [best_negative_channels_pairs(ind).best_negative_value rowsOfMaxes(rows_ind) colsOfMaxes(rows_ind)]; 
                counter = counter + 1;
            end       
            
            if (counter > 10)
                break
            end
        end
    end

end

