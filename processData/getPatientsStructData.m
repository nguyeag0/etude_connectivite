function allPatientsMeg_struct = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path, eegSystem)
%GETPATIENTSSTRUCTDATA
% Get a structure of meg structure per patient
% In parameters, possibility to :
% 1) interpolate the missing electrodes
% 2) re-reference the data according to a chosen electrode or 'avg' or 'small_laplacian'
% 3) swap the electrodes to have the lesion side on the left
    
    % Get input parameters
    parameters = getSetupParameters();

    % Load all patients MEG data file
%         patients_data_path = [current_dir data_dir];
    allPatientsMeg_struct = getAllPatientsMegData( current_dir, data_dir );
        % zeemg(allPatientsMeg_struct(2).megdata.latency, allPatientsMeg_struct(1).megdata.data);

    % Biosemi Add the reference A1 electrode which is at 0 to have all the electrodes
    if(strcmp(eegSystem, 'biosemi'))
        allPatientsMeg_struct = addA1ReferenceToAllPatientsMegData(allPatientsMeg_struct);
    % EGI Add the 204th reference electrode which is at 0 to have all the electrodes
    elseif(strcmp(eegSystem, 'egi'))
        allPatientsMeg_struct = addReference204ToAllPatientsMegData(allPatientsMeg_struct);
    else
        cprintf('blue', 'Please notify the system used (biosemi, egi)');
    end
    
    % Interpolate all the missing electrodes of patient's EEG data
    if(parameters.interpolate_data)
        allPatientsMeg_struct = interpolate_all_megdata( allPatientsMeg_struct, origine_chanloc_file_path );
%         close all;  
    end

    % Re reference the eeg !!!
    if(~isempty(parameters.rereference_data))
        allPatientsMeg_struct = rereference_all_megdata( allPatientsMeg_struct, parameters.rereference_data );
    end

    % Put the electrode of the lesion side on the left
    if(parameters.swap_data)
        allPatientsMeg_struct = transferEegToLeftLesionSide( allPatientsMeg_struct, parameters.swap_data );
    end

%     T = struct2table(allPatientsMeg_struct);
%     sortedT = sortrows(T, 'subjectindex'); 
%     allPatientsMeg_struct = table2struct(sortedT);

    
end