function [ output_allPatientsMeg_struct ] = transferEegToLeftLesionSide( allPatientsMeg_struct, sideTobeSwappedTo )
%TRANSFEREEGTOLEFTLESIONSIDE transfer electrodes to the left lesion side
%   Detailed explanation goes here

% load('data\subject_lesion_side.mat');
load('data\subject_lesion_side_patient.mat');
load('data\symmetricalLabelsBiosemi.mat');
output_allPatientsMeg_struct = allPatientsMeg_struct;

% if sideTobeSwappedTo = 1 : swap to the left side
if(sideTobeSwappedTo==1)
    cprintf('blue', 'Swap EEG data to the left.\n');
    for index=1:length(subject_lesion_side)
        if(strcmp(subject_lesion_side(index).lesion_side, 'D'))
            output_allPatientsMeg_struct = swapData(subject_lesion_side, index, output_allPatientsMeg_struct, sensor_labels);
        end
    end
% if sideTobeSwappedTo = 2 : swap to the right side    
elseif(sideTobeSwappedTo==2)
    cprintf('blue', 'Swap EEG data to the right.\n');
    for index=1:length(subject_lesion_side)
        if(strcmp(subject_lesion_side(index).lesion_side, 'G'))
            output_allPatientsMeg_struct = swapData(subject_lesion_side, index, output_allPatientsMeg_struct, sensor_labels);
        end
    end
end

% if sideTobeSwappedTo = 2 : swap to the right side

end

function [output_allPatientsMeg_struct] = swapData(subject_lesion_side, index, output_allPatientsMeg_struct, sensor_labels)

cprintf('blue', 'patient %d, lesions %c, permuting electrodes side \n', subject_lesion_side(index).patient_index,  subject_lesion_side(index).lesion_side);
output_allPatientsMeg_struct(index).megdata.is_eeg_permuted = true;
swapped_meg = swapChannelsData( output_allPatientsMeg_struct(index).megdata, sensor_labels );
output_allPatientsMeg_struct(index).megdata.swapped_data = swapped_meg.swapped_data;
output_allPatientsMeg_struct(index).megdata.data = swapped_meg.swapped_data;

end