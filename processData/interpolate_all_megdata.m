function [ output_allPatientsMeg_struct ] = interpolate_all_megdata( allPatientsMeg_struct, origine_chanloc_file_path )
%INTERPOLATE_ALL_MEGDATA Summary of this function goes here
%   Detailed explanation goes here

    output_allPatientsMeg_struct = allPatientsMeg_struct;
   
    numberOfTotalChannels = length(output_allPatientsMeg_struct(1).megdata.sensor_labels);
    
    for index=1:size(output_allPatientsMeg_struct, 2)
        cprintf('blue', 'Interpolating MEG data Patient %d.\n', index);
   
        if( length(output_allPatientsMeg_struct(index).megdata.goodchannels) == numberOfTotalChannels )
            continue
        end
        % Interpolate data
        interpolated_meg = interpolate_missing_channels(output_allPatientsMeg_struct(index).megdata, output_allPatientsMeg_struct(index).subjectindex, origine_chanloc_file_path);
        output_allPatientsMeg_struct(index).megdata.interpolated_data = interpolated_meg.interpolated_data;
        output_allPatientsMeg_struct(index).megdata.data = interpolated_meg.interpolated_data;
        output_allPatientsMeg_struct(index).megdata.goodchannels = 1:numberOfTotalChannels;
    end
end

