function [ meg ] = eegref_smallLaplacian( meg, chanlocs_filename )
%EEGREF_SMALLLAPLCIAN  For each channels get the closest channels
%   For each channels get the closest channels according to small Laplacian
%   setup
%   For each channels, considered all electrodes that are at most 30mm
%   away.
%  Spatial filter selection for EEG-based communication:
% https://www.sciencedirect.com/science/article/pii/S0013469497000222
%   
%   For each channel u index i:
%   u_i^LAP = u_i - S_i
%   S_i = sum_jinS_i(h_ij*uj)
%   u_i^LAP = u_i - sum_jinS_i(h_ij*uj)
%   h_ij = (1/d_ij) / sum_jinS_i(1/d_ij)

    laplacianClosestChannelsStruct = getClosestChannelsStruct( chanlocs_filename );
    meg.dataBeforeSlap = meg.data;
    data = meg.data;

%% Iterate on the 300 epoch and 512 time and channels, data per data
%     for epoch_i = 1:size(data, 3)
%         for time_i = 1:size(data, 1)
% 
%             tmp_lap_channels_time_i_epoch_i = [];
%             for channel_i = 1:size(data, 2)
%                 % Gather variables
%                 channels_time_i_epoch_i = data( time_i, :,epoch_i);                
%                 surrounding_i = laplacianClosestChannelsStruct(meg.goodchannels(channel_i));
% 
%                 u_i = data(time_i, channel_i, epoch_i);
%                 S_i = getChannelSurroundingVoltage(channel_i, surrounding_i, channels_time_i_epoch_i, meg );
%                 uLap_i = u_i - S_i;
% 
%                 tmp_lap_channels_time_i_epoch_i = [tmp_lap_channels_time_i_epoch_i, uLap_i];
%             end
% 
%             data( time_i, :,epoch_i) = tmp_lap_channels_time_i_epoch_i;
%         end
% 
%     end

%% Iterate on matrix of channels x 512 x 300, channels per channels
    for channel_i = 1:size(data, 2)
        clearvars channel_i_matrix surrounding_i j_volt_data Uij Hij S_i UiLap;
        
        % Get u_i : 512 x 300 matrix of channel i
        channel_i_matrix = data(:, channel_i, :);
        channel_i_matrix = squeeze(channel_i_matrix);
        
        % Get information of channel i surrounding
        surrounding_i = laplacianClosestChannelsStruct(channel_i);
        surrounding_i.closest_distance(1) = [];
        surrounding_i.closest_labels_index(1) = [];
        surrounding_i.closest_labels(1) = [];
        
        % Get channel j volt data, no need of taking the "goodchannels data", must
        % interpolate and have data with 128 channels before rereferencing
        % with Laplacian.
        j_volt_data = data(:,surrounding_i.closest_labels_index,:);
        Uij = reshape(j_volt_data, [size(j_volt_data, 1) size(j_volt_data, 3) size(j_volt_data, 2)]);

        for h=1:length(surrounding_i.h_ij)
            Hij(:, :, h) = repmat(surrounding_i.h_ij(h), size(Uij, 1), size(Uij, 2));
        end

        S_i = Hij.*Uij;
        S_i = sum(S_i, 3);
        
        UiLap = channel_i_matrix - S_i;
        UiLap = reshape(UiLap, [size(channel_i_matrix, 1) 1 size(channel_i_matrix, 2)]);
        data(:, channel_i, :) = UiLap;
    end

%%
    meg.data = data;
    meg.re_refdata = data;

end

