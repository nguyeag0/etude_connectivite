function [IC_3pairs_matrix, Zscore_matrix] = get_ZScore_n_imagCoh_channelTypeAllPatients_matrix(allPatientsMeg_struct, COMPS, typeChannelsMatrix)
%GET_ZSCORE_N_IMAGCOH_CHANNELTYPEALLPATIENTS_MATRIX Summary of this function goes here
%   Detailed explanation goes here

allConsideredPairsIndex = reshape(typeChannelsMatrix', [size(typeChannelsMatrix, 1)*size(typeChannelsMatrix, 2), 1]);
allConsideredPairs = [COMPS(allConsideredPairsIndex,1) COMPS(allConsideredPairsIndex,2)];

IC_3pairs_matrix = nan(length(allConsideredPairsIndex),length(allPatientsMeg_struct));
Zscore_matrix = nan(size(typeChannelsMatrix, 1), size(typeChannelsMatrix, 2), length(allPatientsMeg_struct));

for index=1:length(allPatientsMeg_struct)
    data = allPatientsMeg_struct(index).megdata.data;
    SC = fcm_sensorccohere31(512,data,allConsideredPairs, 'b',[8 12]);
    IC_3pairs = abs(imag(SC.coh));
    IC_3pairs_reshape = [IC_3pairs(1:3:end) IC_3pairs(2:3:end) IC_3pairs(3:3:end)]; % each line corresponds to the pairs of 3 [ipsi contra trans]
    Z = ( IC_3pairs_reshape - mean(IC_3pairs_reshape, 2) ) ./ std(IC_3pairs_reshape, 0, 2);
    IC_3pairs_matrix(:,index) = IC_3pairs;
    Zscore_matrix(:,:,index) = Z;
end

end


