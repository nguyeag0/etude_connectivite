function [selected_channels_struct] = getBestSixChannels_option4(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path)
%GETBESTSIXCHANNELS_OPTION2 get best 3 pairs (6 channels) based on correlation with source Z-score of IC
%   Look at best of each type comprehensively 
%   pair_indexes iterate on all ipsilesional, then all contralesional, then all transcallosal
%   COMPS: combination structure with all combinations of given channels,
%   their type (ipsi-contra lesional or transcallosal)
%   good_pfdr: selected channels to start with from previous EEG processing
%   allPatientsMeg_struct: subject EEG raw data
%   inverse_correlation_Zscore: respective source IC Zscore

tic

% Get all ipsi, contra and transcallosal pairs
ipsi_pairs = find(COMPS(:,4)==1);
contra_pairs = find(COMPS(:,4)==2);
trans_pairs = find(COMPS(:,4)==3);

% Declare variables
selected_channels_struct = struct();
correlation_result_list = [];
p_list = [];
iterations = [];
pair_indexes_list = [];
f1 = figure;
cycle = 5;

for iteration = 1:cycle     
% Get best transcallosal pair
    % Get transcallosal searched matrix
    if (iteration == 1)
        random_ipsi_pair = ipsi_pairs( randi( length(ipsi_pairs)) );
        random_contra_pairs = contra_pairs( randi( length(contra_pairs)) );
        search_trans_pairs_matrix = [repmat(random_ipsi_pair, length(trans_pairs), 1)...
                                repmat(random_contra_pairs, length(trans_pairs), 1)...
                                trans_pairs];
    else
        search_trans_pairs_matrix = [repmat(best_ipsi_pair, length(trans_pairs), 1)...
                                repmat(best_contra_pair, length(trans_pairs), 1)...
                                trans_pairs];
    end  
    
    % Get IC and Zscore of trans_matrix
    [IC_3pairs_matrix, Zscore_matrix] = get_ZScore_n_imagCoh_channelTypeAllPatients_matrix(allPatientsMeg_struct, COMPS, search_trans_pairs_matrix);
    -->>>>> regstats sur une matrice?
    regression_result = regstats(inverse_correlation_Zscore,Zscore_matrix','linear','adjrsquare');
    [betas_current,~,~,~,stats_current]=regress(inverse_correlation_Zscore,[ones(10,1) Zscore_matrix_current' ]) %,'linear',{'adjrsquare' 'beta'});
    [M,I] = max(correlation_result);
    
    best_trans_pair = trans_pairs(I);
    

    if (iteration == 1)    
        best_pair_indexes = [random_ipsi_pair  random_contra_pairs best_trans_pair];
        best_iter = iteration;
        best_correlation_result = correlation_result;
        best_p = p(I);
    end

% Get best contralateral pair
    % Get contralateral searched matrix   
    if (iteration == 1)
        search_contra_pairs_matrix = [repmat(random_ipsi_pair, length(contra_pairs), 1)...
                                contra_pairs...
                                repmat(best_trans_pair, length(contra_pairs), 1)];
    else
        search_contra_pairs_matrix = [repmat(best_ipsi_pair, length(contra_pairs), 1)...
                                contra_pairs...
                                repmat(best_trans_pair, length(contra_pairs), 1)];
    end

    % Get IC and Zscore of trans_matrix
    [IC_3pairs_matrix, pseudo_Zscore_matrix] = get_ZScore_n_imagCoh_channelTypeAllPatients_matrix(allPatientsMeg_struct, COMPS, search_contra_pairs_matrix);
    regression_result = regstats(inverse_correlation_Zscore,Zscore_matrix_current','linear','adjrsquare');
    [betas_current,~,~,~,stats_current] = regress(inverse_correlation_Zscore,[ones(10,1) Zscore_matrix_current' ]) %,'linear',{'adjrsquare' 'beta'});
        
    best_contra_pair = contra_pairs(I);

% Get best ipsilateral pair
    % Get ipsilateral searched matrix               
    search_ipsi_pairs_matrix = [ipsi_pairs...
                                repmat(best_contra_pair, length(ipsi_pairs), 1)...
                                repmat(best_trans_pair, length(ipsi_pairs), 1)];

    % Get IC and Zscore of trans_matrix
    [IC_3pairs_matrix, pseudo_Zscore_matrix] = get_ZScore_n_imagCoh_channelTypeAllPatients_matrix(allPatientsMeg_struct, COMPS, search_contra_pairs_matrix);
    regression_result = regstats(inverse_correlation_Zscore,Zscore_matrix_current','linear','adjrsquare');
    [betas_current,~,~,~,stats_current]=regress(inverse_correlation_Zscore,[ones(10,1) Zscore_matrix_current' ]) %,'linear',{'adjrsquare' 'beta'});
        
    best_ipsi_pair = ipsi_pairs(I);
    
    best_current_pair = [best_ipsi_pair best_contra_pair best_trans_pair];
    
% Test the correlation_result   
   if( M > best_correlation_result )
       best_correlation_result = M;
       best_p = p(I);
       best_iter = iteration; 
       best_pair_indexes = best_current_pair;
   end

    % Add to lists if interesting
    iterations = [iterations iteration];
    correlation_result_list = [correlation_result_list, M];
    p_list = [p_list, p(I)];
    pair_indexes_list = [pair_indexes_list; best_current_pair]; 
    
     % Plot corr, p, and best channel pairs combination
    figure(f1);    
    plot(iterations, correlation_result_list, 'g:o', iterations, p_list, 'b:o', 'LineWidth', 2);
    xlim([0 cycle ]);
    ylim([-1 1]);    
    legend('corr','p');
    
%     plot_title = ['Correlation = ', num2str(best_correlation_result), ' | p = ', num2str(best_p)];
%     topoplot_pairedChannels(pair_COMPS, best_pair_indexes, best_correlation_result, origine_chanloc_file_path, plot_title);

end
    plot_title = ['Correlation = ', num2str(best_correlation_result), ' | p = ', num2str(best_p)];
    topoplot_pairedChannels(pair_COMPS, best_pair_indexes, best_correlation_result, origine_chanloc_file_path, plot_title);

% Save data
selected_channels_struct.correlations = correlation_result_list';
selected_channels_struct.p = p_list';
selected_channels_struct.pair_indexes_list = pair_indexes_list;
selected_channels_struct.origine_chanloc_file_path = origine_chanloc_file_path;
save('data\selected_channels_struct4.mat', 'selected_channels_struct');

toc


end

