function [selected_channels_struct] = getBestSixChannels_option3(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path)
%GETBESTSIXCHANNELS_OPTION1 get best 3 pairs (6 channels) based on correlation with source Z-score of IC
%   pair_indexes on multiple iterations until find the optimum
%   COMPS: combination structure with all combinations of given channels,
%   their type (ipsi-contra lesional or transcallosal)
%   good_pfdr: selected channels to start with from previous EEG processing
%   allPatientsMeg_struct: subject EEG raw data
%   inverse_correlation_Zscore: respective source IC Zscore

%   Method
%   Get 3 random pairs (one per type)
%   Calculate their Imaginary Coherence and Z-score
%   Correlate with source Z-score of IC
%   Get correlation result and p-value
%   Iterate N time by fixing one pair type -> this is 1 cycle. Then change
%   the pair type
%   At each iteration compare : p-value, correslation result or both
%   Get the best out of all the iterations

%   Variation :
%   Perform a regstats
%   regstats(y,X,model) performs a multilinear regression of the responses in y on the predictors in X. 
%   X is an n-by-p matrix of p predictors at each of n observations. 
%   y is an n-by-1 vector of observed responses.
%   Get patients data : Interpolation 1 | Rereferencing 'A1' | Swap 1

% Get IC_6channels_matrix and pseudo z_score_matrix
tic
selected_channels_struct = struct();
regression_result_list = [];
iterations = [];
pair_indexes_list = [];
betas_list = [];
stats_list = [];
max_channel_iter = 5;
channel_type_number = 3;
channel_type_search = 3;
cycle = 2;
f1 = figure;
xlim([0 max_channel_iter*channel_type_number*cycle ]);
ylim([-1 1]);    
vline(0, 'r', '3');
hold on
f2 = figure;

for iteration=1:max_channel_iter*channel_type_number*cycle 
    
    if ( mod(iteration, max_channel_iter) == 0 )
        channel_type_search = mod( (channel_type_search + 1), channel_type_number ) + 1;
        vline(iteration, 'r', num2str(channel_type_search));
        if (exist('best_pair_indexes') == 1)
            pair_indexes_current = best_pair_indexes;
            endw
    end

    if (iteration == 1)
        pair_indexes_current = getThreePairs(COMPS, good_pfdr, 'random', 0);  
        [IC_3pairs_matrix_current, Zscore_matrix_current] = get_zScore_n_imagCoh_6channels_allPatients_matrix( allPatientsMeg_struct, COMPS, pair_indexes_current );
%         inverse_correlation_Zscore : 1 predictor for 10 observations | Z_score_matrix : 10 observed responses
%         regression_result = regstats(inverse_correlation_Zscore,Zscore_matrix_current','linear','adjrsquare');
        [betas_current,~,~,~,stats_current]=regress(inverse_correlation_Zscore,[ones(10,1) Zscore_matrix_current' ]); %,'linear',{'adjrsquare' 'beta'});
        
        best_pair_indexes = pair_indexes_current;
        best_iter = iteration;
        best_regr_result = stats_current(1);
        best_betas = betas_current(2:end);
        %best_stats = stats_current;
    else
        pair_indexes_current = getThreePairs(COMPS, pair_indexes_current, 'random', channel_type_search);               
    end
    
    % Compute IC, Zscore matrix, corr, p : pseudo_Zscore_matrix, correlation_result et p-value issue de la combinaison des trois pairs choisies
    [IC_3pairs_matrix_current, Zscore_matrix_current] = get_zScore_n_imagCoh_6channels_allPatients_matrix( allPatientsMeg_struct, COMPS, pair_indexes_current );
    %regression_result = regstats(inverse_correlation_Zscore,Zscore_matrix_current','linear','adjrsquare');
    [betas_current,~,~,~,stats_current]=regress(inverse_correlation_Zscore,[ones(10,1) Zscore_matrix_current' ]); %,'linear',{'adjrsquare' 'beta'});
    
    % Test the correlation_result   
%        if( (correlation_result_current > best_correlation_result) & (p_current < 0.5))
       if( (stats_current(1) > best_regr_result))
           best_pair_indexes = pair_indexes_current;
           best_iter = iteration;
           best_regr_result = stats_current(1);
           best_betas = betas_current(2:end);
%                plot_title = ['Iteration ', num2str(best_iter), ': correlation = ', num2str(correlation_result), ' | p = ', num2str(p)];
%                topoplot_pairedChannels(pair_COMPS, pair_indexes, correlation_result, origine_chanloc_file, plot_title);
    
    % Add to lists if interesting
%            iterations = [iterations iteration];
%            correlation_result_list = [correlation_result_list, correlation_result_current];
%            pair_indexes_list = [pair_indexes_list; pair_indexes_current];       
%        else
%     % Add NaN to lists if not interesting
%            iterations = [iterations iteration];
%            correlation_result_list = [correlation_result_list, nan];
%            pair_indexes_list = [pair_indexes_list; [nan, nan, nan] ]; 
       end
     
    % Add to lists if interesting
    iterations = [iterations iteration];
    regression_result_list = [regression_result_list stats_current(1)];
    pair_indexes_list = [pair_indexes_list; pair_indexes_current];          
    betas_list = [betas_list betas_current];
    stats_list = [stats_list stats_current];
    
    % Plot regression, and best channel pairs combination
    figure(f1);    
    plot(iterations, regression_result_list, 'g:o', 'LineWidth', 1);
    legend('regr');
    
    figure(f2);
    plot_title = {['Iteration ', num2str(iteration), ': regression = ', num2str(stats_current(1))], ...
        ['Betas ', num2str(betas_current(2)), ' ', num2str(betas_current(3)), ' ', num2str(betas_current(4))]};
    topoplot_individualPairedChannels(pair_COMPS, pair_indexes_current, betas_current, origine_chanloc_file_path, plot_title);

end

% figure(f2);
% plot_title = {['Iteration ', num2str(best_iter), ': regression = ', num2str(best_regr_result)], ...
%         ['Betas ', num2str(best_betas(1)), ' ', num2str(best_betas(2)), ' ', num2str(best_betas(3))]};
% topoplot_individualPairedChannels(pair_COMPS, best_pair_indexes, betas_current, origine_chanloc_file_path, plot_title);

% Get the vertical lines for the corr_p_graph
vert_lines = [];
for i=0:max_channel_iter*channel_type_number*cycle-1 
    if( mod(i, max_channel_iter) == 0 )
        vert_lines = [vert_lines; i];
    end
end
types = repmat([3; 2; 1], [floor(length(vert_lines)/3) ,1]);
vert_lines = [vert_lines types];

% Save data
selected_channels_struct.regressions = regression_result_list';
selected_channels_struct.pair_indexes_list = pair_indexes_list;
selected_channels_struct.betas_list = betas_list;
selected_channels_struct.vert_lines = vert_lines;
selected_channels_struct.origine_chanloc_file_path = origine_chanloc_file_path;
save('data\selected_channels_struct3.mat', 'selected_channels_struct');
toc


end

