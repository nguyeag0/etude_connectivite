function [outputArg1,outputArg2] = getBestSixChannels_option0(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file)
%GETBESTSIXCHANNELS_OPTION0 Summary of this function goes here
%   Detailed explanation goes here
pair_indexes = getThreePairs(COMPS, good_pfdr, 'good_pfdr');
[IC_3pairs_matrix, pseudo_Zscore_matrix] = get_pseudoZScore_n_imagCoh_6channels_allPatients_matrix( allPatientsMeg_struct, COMPS, pair_indexes );

% Correlation: correlation_result and p-value
[correlation_result,p] = corr(pseudo_Zscore_matrix',inverse_correlation_Zscore,'type','spearman','rows','pairwise');
 
% Plot the good pairs with p corrected values pfdr with a line
plot_title = ['Correlation = ', num2str(correlation_result), ' | p = ', num2str(p)];
topoplot_pairedChannels(pair_COMPS, pair_indexes, correlation_result, origine_chanloc_file, plot_title);

end

