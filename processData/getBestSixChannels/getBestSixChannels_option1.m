function [selected_channels_struct] = getBestSixChannels_option1(COMPS, good_pfdr, allPatientsMeg_struct, inverse_correlation_Zscore, pair_COMPS, origine_chanloc_file_path)
%GETBESTSIXCHANNELS_OPTION1 get best 3 pairs (6 channels) based on correlation with source Z-score of IC
%   pair_indexes on multiple iterations until find the optimum
%   COMPS: combination structure with all combinations of given channels,
%   their type (ipsi-contra lesional or transcallosal)
%   good_pfdr: selected channels to start with from previous EEG processing
%   allPatientsMeg_struct: subject EEG raw data
%   inverse_correlation_Zscore: respective source IC Zscore

%   Method
%   Get 3 random pairs (one per type)
%   Calculate their Imaginary Coherence and Z-score
%   Correlate with source Z-score of IC
%   Get correlation result and p-value
%   Iterate N time by fixing one pair type -> this is 1 cycle. Then change
%   the pair type
%   At each iteration compare : p-value, correslation result or both
%   Get the best out of all the iterations

% Get IC_6channels_matrix and pseudo z_score_matrix
tic
selected_channels_struct = struct();
correlation_result_list = [];
p_list = [];
iterations = [];
pair_indexes_list = [];
max_channel_iter = 40;
channel_type_number = 3;
channel_type_search = 3;
cycle = 2;
f1 = figure;
xlim([0 max_channel_iter*channel_type_number*cycle ]);
ylim([-1 1]);    
vline(0, 'r', '3');
hold on

for iteration=1:max_channel_iter*channel_type_number*cycle 
    
    if ( mod(iteration, max_channel_iter) == 0 )
        channel_type_search = mod( (channel_type_search + 1), channel_type_number ) + 1;
        vline(iteration, 'r', num2str(channel_type_search));
        if (exist('best_pair_indexes') == 1)
            pair_indexes_current = best_pair_indexes;
        end
    end

    if (iteration == 1)
        pair_indexes_current = getThreePairs(COMPS, good_pfdr, 'random', 0);  
        [IC_3pairs_matrix_current, pseudo_Zscore_matrix_current] = get_pseudoZScore_n_imagCoh_6channels_allPatients_matrix( allPatientsMeg_struct, COMPS, pair_indexes_current );
        [correlation_result_current,p_current] = corr(pseudo_Zscore_matrix_current',inverse_correlation_Zscore,'type','spearman','rows','pairwise');        
        best_pair_indexes = pair_indexes_current;
        best_iter = iteration;
        best_correlation_result = correlation_result_current;
        best_p = p_current;
    else
        pair_indexes_current = getThreePairs(COMPS, pair_indexes_current, 'random', channel_type_search);               
    end
    
    % Compute IC, Zscore matrix, corr, p : pseudo_Zscore_matrix, correlation_result et p-value issue de la combinaison des trois pairs choisies
    [IC_3pairs_matrix_current, pseudo_Zscore_matrix_current] = get_pseudoZScore_n_imagCoh_6channels_allPatients_matrix( allPatientsMeg_struct, COMPS, pair_indexes_current );
    [correlation_result_current,p_current] = corr(pseudo_Zscore_matrix_current',inverse_correlation_Zscore,'type','spearman','rows','pairwise');
%     correlation_result_FMA = corr(pseudo_Zscore_matrix_current',FMA,'type','spearman','rows','pairwise');
%     -> avec le FMA
%     mean_correlation = tanh(mean(atanh([correlation_result_current correlation_result_FMA]))
% transformation atanh pour rendre la distribution de la corrélation
% "normal" pour que la mean soit plus juste

    % Test the correlation_result   
%        if( (correlation_result_current > best_correlation_result) && (p_current < 0.5))  
     if(correlation_result_current > best_correlation_result)  
           best_p = p_current;
           best_pair_indexes = pair_indexes_current;
           best_iter = iteration; 
           best_correlation_result = correlation_result_current;
%                plot_title = ['Iteration ', num2str(best_iter), ': correlation = ', num2str(correlation_result), ' | p = ', num2str(p)];
%                topoplot_pairedChannels(pair_COMPS, pair_indexes, correlation_result, origine_chanloc_file, plot_title);
    
    % Add to lists if interesting
%            iterations = [iterations iteration];
%            correlation_result_list = [correlation_result_list, correlation_result_current];
%            p_list = [p_list, p_current];
%            pair_indexes_list = [pair_indexes_list; pair_indexes_current];       
%        else
%     % Add NaN to lists if not interesting
%            iterations = [iterations iteration];
%            correlation_result_list = [correlation_result_list, nan];
%            p_list = [p_list, nan];
%            pair_indexes_list = [pair_indexes_list; [nan, nan, nan] ]; 
       end
     
    % Add to lists if interesting
    iterations = [iterations iteration];
    correlation_result_list = [correlation_result_list, correlation_result_current];
    p_list = [p_list, p_current];
    pair_indexes_list = [pair_indexes_list; pair_indexes_current];          
   
    % Plot corr, p, and best channel pairs combination
    figure(f1);    
    plot(iterations, correlation_result_list, 'g:o', iterations, p_list, 'b:o', 'LineWidth', 1);
    legend('corr','p');
end

plot_title = ['Iteration ', num2str(best_iter), ': correlation = ', num2str(best_correlation_result), ' | p = ', num2str(best_p)];
topoplot_pairedChannels(pair_COMPS, best_pair_indexes, best_correlation_result, origine_chanloc_file_path, plot_title);

% Get the vertical lines for the corr_p_graph
vert_lines = [];
for i=0:max_channel_iter*channel_type_number*cycle-1 
    if( mod(i, max_channel_iter) == 0 )
        vert_lines = [vert_lines; i];
    end
end
types = repmat([3; 2; 1], [floor(length(vert_lines)/3) ,1]);
vert_lines = [vert_lines types];

% Save data
selected_channels_struct.correlations = correlation_result_list';
selected_channels_struct.p = p_list';
selected_channels_struct.pair_indexes_list = pair_indexes_list;
selected_channels_struct.vert_lines = vert_lines;
selected_channels_struct.origine_chanloc_file_path = origine_chanloc_file_path;
save('data\selected_channels_struct.mat', 'selected_channels_struct');
toc


end

