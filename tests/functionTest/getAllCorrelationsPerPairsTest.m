function [ output_args ] = getAllCorrelationsPerPairsTest( input_args )
%GETALLCORRELATIONSPERPAIRSTEST Summary of this function goes here
%   Detailed explanation goes here


% Interpolation 1 | Re-reference "no" | Swap 1
allPatientsMeg_struct = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path);
good_pfdr = [7416, 3592, 3836];

% Get inverse source
inverse_correlation_Zscore_file = 'ic_first_rest_preZ_m_target_vx_10p.mat';
load([current_dir 'FC_patients_data\' inverse_correlation_Zscore_file]);
inverse_correlation_Zscore = ic_first_rest_preZ_m_target_vx_10p;
clear ic_first_rest_preZ_m_target_vx_10p inverse_correlation_Zscore_file;

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   
pair_COMPS = COMPS(:, 1:2);

% Get all ipsi, contra and transcallosal pairs
ipsi_pairs = find(COMPS(:,4)==1);
contra_pairs = find(COMPS(:,4)==2);
trans_pairs = find(COMPS(:,4)==3);

allConsideredPairs = [COMPS(ipsi_pairs,1) COMPS(ipsi_pairs,2)];

for index=1:length(allPatientsMeg_struct)
    data = allPatientsMeg_struct(index).megdata.data;
    SC = fcm_sensorccohere31(512,data,allConsideredPairs, 'b',[8 12]);
    IC_3pairs = abs(imag(SC.coh));
    % IC_3pairs_reshape = [IC_3pairs(1:3:end) IC_3pairs(2:3:end) IC_3pairs(3:3:end)]; % each line corresponds to the pairs of 3 [ipsi contra trans]
    % IC_3pairs_matrix(:,index) = IC_3pairs;
    pseudo_Zscore_matrix(:,index) = IC_3pairs;
end

[correlation_result,p] = corr(pseudo_Zscore_matrix',inverse_correlation_Zscore,'type','spearman','rows','pairwise');


% Get values all ipsi corr
plot_title = 'Correlation ipsi pairs';
pair_indexes = ipsi_pairs;
selected_pairs = pair_COMPS(pair_indexes,:);    
values = correlation_result;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file); 
title(plot_title);   

    
% Get values greater than 0.5 or lower than -0.5
goodCorrLogical = (correlation_result>0.5 |correlation_result<-0.5);
I = find((correlation_result>0.5 |correlation_result<-0.5));
goodCorrValues = correlation_result(goodCorrLogical);

plot_title = 'Correlation ipsi pairs greater than 0.5 and lower than -0.5';
pair_indexes = ipsi_pairs(I);
selected_pairs = pair_COMPS(pair_indexes,:);    
values = goodCorrValues;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file); 
title(plot_title);   

% Get values greater than 0.5
goodCorrLogical = correlation_result>0.5;
I = find(correlation_result>0.5);
goodCorrValues = correlation_result(goodCorrLogical);

plot_title = 'Correlation ipsi pairs greater than 0.5';
pair_indexes = ipsi_pairs(I);
selected_pairs = pair_COMPS(pair_indexes,:);    
values = goodCorrValues;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file); 
title(plot_title);

    
% Get values lower than -0.5
goodCorrLogical = correlation_result<-0.5;
I = find(correlation_result<-0.5);
goodCorrValues = correlation_result(goodCorrLogical);

plot_title = 'Correlation ipsi pairs lower than -0.5';
pair_indexes = ipsi_pairs(I);
selected_pairs = pair_COMPS(pair_indexes,:);    
values = goodCorrValues;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_chanloc_file); 
title(plot_title);

end

