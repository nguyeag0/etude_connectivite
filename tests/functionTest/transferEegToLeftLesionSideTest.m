function [] = transferEegToLeftLesionSideTest( allPatientsMeg_struct )
%TRANSFEREEGTOLEFTLESIONSIDETEST Summary of this function goes here
%   Detailed explanation goes here

allPatientsMeg_struct = transferEegToLeftLesionSide( allPatientsMeg_struct );

expected_struct_index_to_permute = [1 4 6 8 10];
expected_patient_index_to_permute = [1 5 8 11 13];
struct_index_to_permute = [];
patient_index_to_permute = [];
patient_number = length(allPatientsMeg_struct);

disp(isfield(allPatientsMeg_struct(1).megdata, 'is_eeg_permuted'));

for index=1:patient_number
    if (isfield(allPatientsMeg_struct(index).megdata, 'is_eeg_permuted'))
        if allPatientsMeg_struct(index).megdata.is_eeg_permuted
            cprintf('blue', 'permuted patient %d\n', allPatientsMeg_struct(index).patientindex);
            struct_index_to_permute = [struct_index_to_permute, index];
            patient_index_to_permute = [patient_index_to_permute, allPatientsMeg_struct(index).patientindex];
        end
    end
end

assert(isempty(setdiff(struct_index_to_permute, expected_struct_index_to_permute)) == 1);
assert(isempty(setdiff(patient_index_to_permute, expected_patient_index_to_permute)) == 1);

end

