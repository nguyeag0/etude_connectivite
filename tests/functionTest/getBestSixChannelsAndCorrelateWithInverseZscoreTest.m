function [  ] = getBestSixChannelsAndCorrelateWithInverseZscoreTest( allPatientsMeg_struct, COMPS, good_pfdr, FMA, origine_chanloc_file_path )
%GETBESTSIXCHANNELSANDCORRELATEWITHINVERSEZSCORETEST Summary of this function goes here
%   Detailed explanation goes here

    inverse_correlation_Zscore_file = 'ic_first_rest_preZ_m_target_vx_10p.mat';
    load([current_dir 'FC_patients_data\' inverse_correlation_Zscore_file]);
    inverse_correlation_Zscore = ic_first_rest_preZ_m_target_vx_10p;
    clear ic_first_rest_preZ_m_target_vx_10p inverse_correlation_Zscore_file;
    
    getBestSixChannelsAndCorrelateWithInverseZscore( allPatientsMeg_struct, COMPS, good_pfdr, FMA , inverse_correlation_Zscore, origine_chanloc_file_path);

end
