function [] = convertEeg_megToEeglabTest( patient_data_path, patient_data_filename )
%% Convert EEG data from MEG to EEGLAB test
cprintf('blue', 'Test : convertEeg_megToEeglabTest\n');

% Load meg structure
load([patient_data_path patient_data_filename]);

eeglab_eegdata = convertEeg_megToEeglab(meg.data);
assert(size(size(eeglab_eegdata), 2) == 2);

end

