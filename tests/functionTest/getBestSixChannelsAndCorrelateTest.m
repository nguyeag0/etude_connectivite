function [ ] = getBestSixChannelsAndCorrelateTest( patient_data_path, current_dir, comps_path, data_dir, origine_chanloc_file_path, COMPS)
%GETBESTSIXCHANNELSANDCORRELATEWITHINVERSEZSCORETEST Summary of this function goes here
%   Detailed explanation goes here

% Interpolation: 1  
allPatientsMeg_struct = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path);
good_pfdr = [7416, 3592, 3836];

% getThreePairsTest(COMPS, good_pfdr);

getBestSixChannelsAndCorrelateWithInverseZscoreTest( allPatientsMeg_struct, COMPS, good_pfdr, FMA, origine_chanloc_file_path );

end
