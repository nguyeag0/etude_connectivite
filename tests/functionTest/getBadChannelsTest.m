function [] = getBadChannelsTest( patient_data_path,  patient_data_filename)
%% Get bad channels function test

cprintf('blue', 'Test : getBadChannelsTest \n');

% Load meg structure
cprintf('blue', 'Test1 : getBadChannelsTest \n');
load([patient_data_path patient_data_filename]);

% Test parameters go through function
cprintf('blue', 'Test2 : getBadChannelsTest \n');
bad_channels = getBadChannels(meg);
assert(size(bad_channels, 1) > 0);
assert(size(bad_channels, 1) <= 128);

% Test bad electrodes more than 10%
% meg.goodchannels = meg.goodchannels(size(meg.), :);
cprintf('blue', 'Test 3: getBadChannelsTest \n');
bad_channels = getBadChannels(meg);
assert(size(bad_channels, 1) > 0);

% Test bad and good electrodes match whole set
cprintf('blue', 'Test 4: getBadChannelsTest \n');
load([patient_data_path patient_data_filename]);
bad_channels = getBadChannels(meg);
all_channels = cat(1, bad_channels, meg.goodchannels);
all_channels = sort(all_channels, 1);
total_channels_index = (1:1:size(meg.sensor_labels, 1))';
channel_differences = setdiff(total_channels_index, all_channels);
assert(size(channel_differences, 1) == 0);

% Test correct bad electrodes retrival
cprintf('blue', 'Test 5 : getBadChannelsTest \n');
load([patient_data_path patient_data_filename]);
all_electrodes_index = (1:1:128);
chosen_bad_electrodes = [2 5 38]; 
chosen_good_electrodes = all_electrodes_index;
chosen_good_electrodes([2 5 38]) = [];
meg.goodchannels = chosen_good_electrodes;
bad_channels = getBadChannels(meg);
channel_differences = setdiff(chosen_bad_electrodes, bad_channels);
assert(size(channel_differences, 2) == 0);

end

