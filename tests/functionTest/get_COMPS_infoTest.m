function [] = get_COMPS_infoTest( )
%GET_COMPS_INFOTEST Summary of this function goes here
%   Detailed explanation goes here

channel_location_filename = 'biosemi128.xyz';
comps_path = 'data\COMPS';

COMPS = get_COMPS_info( comps_path, channel_location_filename );

assert(size(COMPS, 1) == 8128);
assert(size(COMPS, 2) == 4);
end

