function [] = getAllPatientsMegDataTest( patients_data_path )
%GETALLPATIENTSMEGDATATEST Summary of this function goes here
%   Detailed explanation goes here

    allPatientsMeg_struct = getAllPatientsMegData(patients_data_path);
    assert(size(allPatientsMeg_struct, 2) == 10);
    
    for i=1:size(allPatientsMeg_struct, 2)
        assert(isstruct(allPatientsMeg_struct(i)));
    end
    

end

