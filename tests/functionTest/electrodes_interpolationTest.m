function [] = electrodes_interpolationTest( patient_data_path, patient_data_filename )
%ELECTRODES_INTERPOLATIONTEST Summary of this function goes here
%   Detailed explanation goes here

cprintf('blue', 'Test : electrodes_interpolationTest\n');

%% Electrodes interpolation test
load([patient_data_path patient_data_filename]);
meg.data = cat(2,zeros(512,1,300),meg.data);
meg = nut_eegref(meg,'avg');

bad_channels = getBadChannels(meg);
eeglab_structure = convertStruct_megToEeglab( meg );
interpolated_eeglab_struct = electrodes_interpolation(meg, eeglab_structure, bad_channels, 1 );
assert(size(interpolated_eeglab_struct.data, 1) == 128);


end

