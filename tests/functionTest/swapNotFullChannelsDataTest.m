function [] = swapNotFullChannelsDataTest( meg_struct )
%SWAPNOTFULLCHANNELSDATATEST Swap channels data according to cap label
%   Swap channels in needed case to have the same electrodes lesion side.
%   Test for eeg data under 128 electrodes

    load('data\symmetricalLabelsBiosemi.mat');

    meg_struct.data = cat(2,zeros(512,1,300),meg_struct.data);
    meg_struct = nut_eegref(meg_struct,'avg');
    
    swapped_meg = swapChannelsData( meg_struct, sensor_labels );

    % Test good size swapped EEG data

end

