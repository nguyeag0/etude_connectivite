function [] = convertStruct_megToEeglabTest( patient_data_path, patient_data_filename  )
%CONVERTSTRUCT_MEGTOEEGLABTEST Summary of this function goes here
%   Detailed explanation goes here

cprintf('blue', 'Test : convertStruct_megToEeglabTest\n');

% Load meg structure
load([patient_data_path patient_data_filename]);

%% Get EEGLAB structure from eeglab format data
eeglab_structure = convertStruct_megToEeglab( meg );
assert(size(eeglab_structure.data, 1) == size(meg.data, 2) );


end

