function [] = getClosestChannelsStructTest()
%GETCLOSESTCHANNELSSTRUCTTEST Summary of this function goes here
%   Detailed explanation goes here

chanlocs_filename = 'biosemi128.xyz';

% Get xyz location and labels test
xyz = get_xyz_locations_n_labels(chanlocs_filename);
assert(size(xyz.locations, 1) == 128);
assert(size(xyz.labels, 1) == 128);

% Check if size of matrix is 128 x 128, And if diagonal = 0
distance_matrix = squareform(pdist(xyz.locations, 'euclidean'));
assert(size(distance_matrix, 1) == 128);
assert(size(distance_matrix, 2) == 128);

diagMat = diag(distance_matrix);
diagMat(diagMat==0) = [];
assert(isempty(diagMat));

laplacianClosestChannelsStruct = getClosestChannelsStruct( chanlocs_filename );
assert(length(laplacianClosestChannelsStruct) == 128); % autre id�e de test?



end

