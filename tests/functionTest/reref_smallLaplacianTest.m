function [] = reref_smallLaplacianTest( patient_data_path, patient_data_filename )
%REREF_SMALLLAPLACIANTEST Summary of this function goes here
%   Detailed explanation goes here

cprintf('blue', 'Test : reref_smallLaplacianTest\n');

% Load meg structure
chanlocs_filename = 'biosemi128.xyz';
load([patient_data_path patient_data_filename]);
load('data\symmetricalLabelsBiosemi.mat');
subject_index = 1;
meg.data = cat(2,zeros(512,1,300),meg.data);
meg.rawdata = meg.data;

%% Check S_i computation
data = meg.data;

laplacianClosestChannelsStruct = getClosestChannelsStruct( chanlocs_filename );

channel_i = 1;
surrounding_i = laplacianClosestChannelsStruct(meg.goodchannels(1));
S_i = getChannelSurroundingVoltage( channel_i, surrounding_i, data(1, :, 1), meg );

assert(S_i~=0);

%% Check reref with small Laplacian

interpolated_meg = interpolate_missing_channels(meg, 1);
interpolated_meg.data = interpolated_meg.interpolated_data;
meg = interpolated_meg;

tic
reref_smallLap_meg = eegref_smallLaplacian( interpolated_meg, chanlocs_filename );
duration = toc;
disp(duration);

% Plot eeg before interpolation and small laplacian rereferencing
EEG = convertStruct_megToEeglab( meg );
% Create a temporary variable to store the data up to 128 electrodes
data = zeros(size(meg.sensor_labels, 1), EEG.pnts);
data(meg.goodchannels,:) = EEG.data;
figure_title = strcat('ORIGINAL', ' 1');
figure; topoplot(data(:,fix(EEG.pnts/2)),EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);

% Plot eeg after interpolation and small laplacian rereferencing
interp_EEG = convertStruct_megToEeglab( interpolated_meg );
% Create a temporary variable to store the data up to 128 electrodes
data= interp_EEG.data;
figure_title = strcat('INTERPOLATED', ' 1');
figure; topoplot(data(:,fix(interp_EEG.pnts/2)),EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);

% Plot eeg after interpolation and small laplacian rereferencing
% Version 1, iterating on 3 dimensions (epoch, time, channels)
reref_EEG = convertStruct_megToEeglab( reref_smallLap_meg_v1 );
% Create a temporary variable to store the data up to 128 electrodes
data = reref_EEG.data;
figure_title = strcat('REREF', ' V1');
figure; topoplot(data(:,fix(reref_EEG.pnts/2)),reref_EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);

% Version 2, iterating on 1 dimension (channels)
reref_EEG = convertStruct_megToEeglab( reref_smallLap_meg_v2 );
% Create a temporary variable to store the data up to 128 electrodes
data = reref_EEG.data;
figure_title = strcat('REREF', ' V2');
figure; topoplot(data(:,fix(reref_EEG.pnts/2)),reref_EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);

% Version 2, iterating on 1 dimension (channels)
reref_EEG = convertStruct_megToEeglab( reref_smallLap_meg );
% Create a temporary variable to store the data up to 128 electrodes
data = reref_EEG.data;
figure_title = strcat('REREF', ' V2');
figure; topoplot(data(:,fix(reref_EEG.pnts/2)),reref_EEG.chanlocs,'electrodes','labels','emarker',{'*','k',32,1},'numcontour',12); colorbar; title(figure_title);




% Test equality of both matrices
res1 = isequal(reref_smallLap_meg_v1.data, reref_smallLap_meg_v2.data);
res2 = reref_smallLap_meg_v1.data == reref_smallLap_meg_v2.data;
res3 = reref_smallLap_meg_v1.data - reref_smallLap_meg_v2.data;
res3 = reref_smallLap_meg_v1.data(:, 1, :) - reref_smallLap_meg_v2.data(:, 1, :);
res4 = find(reref_smallLap_meg_v1.data ~= reref_smallLap_meg_v2.data);

zeemg(meg.latency, meg.data);



end

