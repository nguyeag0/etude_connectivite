function [] = swapChannelsDataTest( patients_data_path, patient_data_path, patient_data_filename )
%SWAPCHANNELSDATATEST Swapping Channels Data Test function test
%   Detailed explanation goes here
cprintf('blue', 'Test : swapChannelsDataTest\n');

% Load meg structure
load([patient_data_path patient_data_filename]);
load('data\symmetricalLabelsBiosemi.mat');
subject_index = 1;
meg.data = cat(2,zeros(512,1,300),meg.data);
meg.rawdata = meg.data;
meg = nut_eegref(meg,'avg');

% Get swapped data with interpolation
interpolated_meg = interpolate_missing_channels(meg, subject_index);
interpolated_meg.data = interpolated_meg.interpolated_data;
swappedInterpolated_meg = swapChannelsData( interpolated_meg, sensor_labels );


%% Test structure good size of swapped EEG data

swapped_data = swappedInterpolated_meg.swapped_data;

assert(size(swapped_data,1) == size(meg.data,1));
assert(size(swapped_data,2) == size(meg.sensor_labels, 1));
assert(size(swapped_data,3) == size(meg.data,3));


%% Test good channels labels match swap on all set

swapped_data = swappedInterpolated_meg.swapped_data;
good_channels = swappedInterpolated_meg.goodchannels;
eeglab_data = convertEeg_megToEeglab(interpolated_meg.interpolated_data);
eeglab_swapped_data = convertEeg_megToEeglab(swapped_data);

for index=1:size(good_channels)
%         fprintf('actual channel index - symmetrical channel index: %d - %d\n',...
%                good_channels(index), sensor_labels{good_channels(index), 3});
    channel_differences = setdiff( eeglab_swapped_data(sensor_labels{good_channels(index), 3},:) ,...
                                        eeglab_data(index, :) ); 
    assert(size(channel_differences, 2) == 0);                                
end


%% Test good channels data match swap on all set
interpolated_data = interpolated_meg.interpolated_data;
swapped_data = swappedInterpolated_meg.swapped_data;
good_channels = swappedInterpolated_meg.goodchannels;

for index=1:size(good_channels)
    matched_original_index = sensor_labels{index, 3};
    equality_result = swapped_data(:, index, :) == interpolated_data(:,matched_original_index,:);
    equality_result(equality_result == 1) = [];
    assert(isempty(equality_result) == 1 );
end


%% Permutation function test on all megs_struct

allPatientsMeg_struct = getAllPatientsMegData(patients_data_path);
allPatientsMeg_struct = addA1ReferenceToAllPatientsMegData(allPatientsMeg_struct);
allPatientsMeg_struct = interpolate_all_megdata( allPatientsMeg_struct );
transferEegToLeftLesionSideTest( allPatientsMeg_struct );

%% Permutation function test on one eeg data set that has less than 128

% Get swapped data without interpolation
swappedNotInterpolated_meg = swapChannelsData( meg, sensor_labels );

swapped_data = swappedNotInterpolated_meg.swapped_data;
data = meg.data;

% Check if raw data and swapped data have the same size
assert(size(meg.data, 1) == size(swapped_data, 1));
assert(size(meg.data, 2) == size(swapped_data, 2));
assert(size(meg.data, 3) == size(swapped_data, 3));

% Get the good_channels array and the sorted matched swapped array
good_channels = meg.goodchannels;
sorted_matched_good_channels = sort(cell2mat(sensor_labels(good_channels, 3)));

% Check if row of meg.data corresponds to the swapped row in swapped_data
for index = length(good_channels)
    matched_swapped_index = find( sorted_matched_good_channels == [sensor_labels{good_channels(index), 3}] );
    equality_result = meg.data(:,index,:)== swapped_data(:, matched_swapped_index, :);
    equality_result(equality_result == 1) = [];
    assert(isempty(equality_result) == 1 );
end


% Check if row of swapped_data corresponds to the original row in meg.data
for index = length(sorted_matched_good_channels)
    matched_original_index = find( good_channels == sensor_labels{sorted_matched_good_channels(index), 3} );
    equality_result = swapped_data(:, index, :) == meg.data(:,matched_original_index,:);
    equality_result(equality_result == 1) = [];
    assert(isempty(equality_result) == 1 );
end

end

