function [] = convertEeg_eeglabToMegTest( patient_data_path, patient_data_filename )
%CONVERTEEG_EEGLABTOMEGTEST Summary of this function goes here

cprintf('blue', 'Test : convertEeg_eeglabToMegTest\n');

% Load meg structure
load([patient_data_path patient_data_filename]);

%% Convert EEG data from EEGLAB to MEG test
sampling_rate = 512;
epoch_quantity = 300;
eeglab_eegdata = convertEeg_megToEeglab(meg.data);
data = convertEeg_eeglabToMeg(eeglab_eegdata, sampling_rate, epoch_quantity);
assert(size(meg.data, 1) == size(data, 1));
assert(size(meg.data, 2) == size(data, 2));
assert(size(meg.data, 3) == size(data, 3));

result_diff = meg.data==data;
assert( size(find(result_diff == 0), 1) == 0 );

end

