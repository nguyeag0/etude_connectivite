function [ ] = interpolate_all_megdataTest( allPatientsMeg_struct )
%INTERPOLATE_ALL_MEGDATATEST Summary of this function goes here
%   Detailed explanation goes here
allPatientsMeg_struct = interpolate_all_megdata( allPatientsMeg_struct );

for index=1:length(allPatientsMeg_struct)
    assert(isfield(allPatientsMeg_struct(index).megdata, 'interpolated_data') == 1);
end

end

