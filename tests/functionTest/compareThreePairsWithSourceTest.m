function [] = compareThreePairs(patient_data_path, current_dir, comps_path, data_dir, origine_chanloc_file_path, COMPS, FMA)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Get Patients data
allPatientsMeg_struct = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path);

% Get good pairs
good_pfdr = [7416, 3592, 3836];

% Get source data
inverse_correlation_Zscore_file = 'ic_first_rest_preZ_m_target_vx_10p.mat';
load([current_dir 'FC_patients_data\' inverse_correlation_Zscore_file]);
inverse_correlation_Zscore = ic_first_rest_preZ_m_target_vx_10p;
clear ic_first_rest_preZ_m_target_vx_10p inverse_correlation_Zscore_file;
     
% Get the IC and Zscore as if it came from only 6 channels
for patient=1:length(allPatientsMeg_struct)
    megdata = allPatientsMeg_struct(patient).megdata;
    % Get data from 6 channels of patient's EEG 128 data
    megdata.data = megdata.data(:,[COMPS(good_pfdr(1),1) COMPS(good_pfdr(1),2)...
                                    COMPS(good_pfdr(2),1) COMPS(good_pfdr(2),2)...
                                    COMPS(good_pfdr(3),1) COMPS(good_pfdr(3),2)],:);
   % Update the "good" 6 channels
    megdata.goodchannels = [COMPS(good_pfdr(1),1) COMPS(good_pfdr(1),2)...
                            COMPS(good_pfdr(2),1) COMPS(good_pfdr(2),2)...
                            COMPS(good_pfdr(3),1) COMPS(good_pfdr(3),2)];         
    megdata = nut_eegref(megdata, 'AVG');
        
    SC = fcm_sensorccohere31(512,megdata.data,[1 2;3 4;5 6],'b',[8 12]);
    IC_3pairs = abs(imag(SC.coh));
    IC_3pairs_matrix(:,patient) = IC_3pairs;
%     pseudo_Zscore(:,patient) = IC_3pairs(1) - IC_3pairs(3); % pair_ipsi - pair_trans
    pseudo_Zscore(:,patient) = IC_3pairs(1) - mean(IC_3pairs(2:3)); % pair_ipsi - mean(pair_contr, pair_trans)
%     pseudo_Zscore_matrix = IC_3pairs(3) - mean(IC_3pairs(1:2));
end

[correlation_result,p] = corr(pseudo_Zscore',inverse_correlation_Zscore,'type','spearman','rows','pairwise');


end

