function [] = interpolate_missing_channelsTest( patient_data_path, patient_data_filename )
%INTERPOLATE_MISSING_CHANNELSTEST Summary of this function goes here
%   Detailed explanation goes here

cprintf('blue', 'Test : interpolate_missing_channelsTest\n');

%% Interpolation function test

% Load meg structure
load([patient_data_path patient_data_filename]);
meg.data = cat(2,zeros(512,1,300),meg.data);
meg = nut_eegref(meg,'avg');

interpolated_meg = interpolate_missing_channels(meg, 1);

assert(size(interpolated_meg.data, 2) == size(meg.data, 2));        
assert(size(interpolated_meg.interpolated_data, 2) == 128);
assert(size(interpolated_meg.goodchannels, 1) == 128);

end

