a = 0;

if(a)
    disp('yes');
elseif(~a)
    disp('no');
end
%%
p = input('choose ', 's');

%%
data = meg.data;

myindex = [1, 10, 5];
mydata = data(:, myindex, :);

isequal(data(:, 5, :), mydata(:, 3, :));

e = cell2mat(corresponding_swapped_good_channels)
f = sort(e)

a = setdiff(good_channels, f)

%%

a = eye(3);
a(1, 1) = 0
b=a;
b(:, all(a==0, 2)) = []
k(:, all(swapped_data==0,2),:)=[];

%%
good_channels=[1,5,6,10];

% iterate on element
for index=good_channels(:)'
    disp(index)
end

%%
x = electrode1;
% remove element from an array
x(x==0) = [];

% keep element of an array regarding a condition
y = x(x<30);

%%

% Calcule distance entre deux �lectrodes
clear;
filename = 'biosemi128.xyz';
xyz = xyzread(filename, 'headerlines', 1);

% Get matrix of electrode distances
distance_matrix = squareform(pdist(xyz.locations, 'euclidean'));

% Get the closest electrode
electrodeA1 = distance_matrix(1,:);
x = electrodeA1;

n = 5;
[xs, index] = sort(x);
closest_distance = xs(1:n);
closest_distance = closest_distance(closest_distance<=30.5)
n = length(closest_distance);
result_index = index(1:n);
closest_labels = xyz.labels(result_index);

laplacianClosestChannelsStruct(1).closest_distance = closest_distance;
laplacianClosestChannelsStruct(1).closest_labels_index = result_index;
laplacianClosestChannelsStruct(1).closest_labels = closest_labels;


electrodeD23 = distance_matrix(119,:);
x = electrodeD23;

n = 5;
[xs, index] = sort(x);
closest_distance = xs(1:n);
closest_distance = closest_distance(closest_distance<=30.5)
n = length(closest_distance);
result_index = index(1:n);
closest_labels = xyz.labels(result_index);

laplacianClosestChannelsStruct(2).closest_distance = closest_distance ;
laplacianClosestChannelsStruct(2).closest_labels_index = result_index;
laplacianClosestChannelsStruct(2).closest_labels = closest_labels;

%%

filename = 'biosemi128.xyz';
laplacianClosestChannelsStruct = getClosestChannelsStruct( filename );

%%
a = [1, 2, 3 ,4];
b = 1./a;

%%
data = ones(3);
data = ones(2, 3, 4);
k = 2;
size(k)
k_mat = repmat(k, 2, 3);
res=k_mat.*data;

km1 = k_mat
km1(:, :,2) = k_mat
km1(:, :,3) = k_mat
km1(:, :,4) = k_mat
res = km1.*data

one = repmat(1, 2, 3)
two = repmat(2, 2, 3)
three = repmat(3, 2, 3)
four = repmat(4, 2, 3)

data = one
data(:,:,2)=two
data(:,:,3)=three
data(:,:,4)=four

d = reshape(data, [3 2 4])

d2 = repmat(1, 3, 2)
d2(:, :, 2) = one2
d2(:, :, 3) = one2
d2(:, :, 4) = one2

res=d-d2
r=reshape(res, [2 3 4])

d3(:, :, 1) = one2
r=reshape(d3, [3 1 2])
size(r)

%%
a = isequal(datav1, datav2);

%%
comps = nchoosek(1:5,2)

zScore_matrix = nan(length(comps),10);
x = [1, 1, 1]
i = [2, 5, 7]

zScore_matrix(i,1) = x;

%%
% ouvrir allPatientsMeg_struct et ic_first_rest_preZ_blabla
% interpolation
% swap to lesion side

% define possible elec pairs
filename = 'biosemi128.xyz';
xyz = xyzread(filename, 'headerlines', 1);
comps_path = 'data\COMPS';
load(comps_path);
distance = pdist(xyz.locations, 'euclidean')';
isipsi = find(xyz.locations(:,1)<-10);
Cipsi = COMPS(ismember(COMPS(:,1),isipsi) & ismember(COMPS(:,2),isipsi) & distance > 60,:);
iscontra = find(xyz.locations(:,1)>10);
Ccontra = COMPS(ismember(COMPS(:,1),iscontra) & ismember(COMPS(:,2),iscontra) & distance > 60,:);
% + transcallosal

% loop: for k=1: num_possibles_pairs
%   extraire 6 electrodes (3 pairs) selon crit�res
%   Ref AVG des 6 elecs
%   calculer IC: 1-2 = pairs "n�gatifs", 3 = pair "positiv"
%   IC_pseudo_Z = IC(3,:)-mean(IC(1:2,:))  % remplace le z-score
%   [r(k),p(k)]=corr(IC_pseudo_Z',ic_first_rest_preZ_m_target_vx_10p,'type','spearman')
% end
% find 10 meilleures r (pour voir aussi la stabilit�)

load(comps_path);

% scatter3 plot
d=0.1;
scatter3(xyz.locations(:, 1), xyz.locations(:, 2), xyz.locations(:, 3), 'filled', 'MarkerFaceColor', [1 1 1]);
text(xyz.locations(:, 1)+d, xyz.locations(:, 2)+d, xyz.locations(:, 3)+d, xyz.labels);
rotate3d on;

scatter3(xyz.locations(isipsi, 1), xyz.locations(isipsi, 2), xyz.locations(isipsi, 3), 'filled', 'MarkerFaceColor',[0 .75 .75]);
text(xyz.locations(:, 1)+d, xyz.locations(:, 2)+d, xyz.locations(:, 3)+d, xyz.labels);

scatter3(xyz.locations(iscontra, 1), xyz.locations(iscontra, 2), xyz.locations(iscontra, 3), 'filled', 'MarkerFaceColor',[1 0 0]);
text(xyz.locations(:, 1)+d, xyz.locations(:, 2)+d, xyz.locations(:, 3)+d, xyz.labels);

% topoplot_connect
figure;
display_structure = struct();
display_structure.chanPairs = COMPS(channel_transcallosal_label,:);
display_structure.connectStrength = ones(length(channel_transcallosal_label));
% display_structure.connectStrengthLimits = [0.6 1];
topoplot_connect(display_structure, filename)

%%
data = allPatientsMeg_struct(1).megdata.data;
SC = fcm_sensorccohere31(512,data,'all','b',[8 12]);

data1 = allPatientsMeg_struct(1).megdata.data(:,1:2,:);
data2 = allPatientsMeg_struct(1).megdata.data(:,3:4,:);

SC = fcm_sensorccohere31(512,data1,,'b',[8 12]);
SC = fcm_sensorccohere31(512,[data1;data2],'all','b',[8 12]);

tic
SC1 = fcm_sensorccohere31(512,data,[90 119;33 57;35 114],'b',[8 12]);
toc
pairs_matrix1 = [90 119;33 57;35 114];
pairs_matrix2 = [80 109;23 47;25 104];

% nombre de seconde pour calculer SC sur toutes les possibilt�s
temps = 0.291711 * 3000/60
% fcm_sensorccohere31 on matrix of channels


SC = fcm_sensorccohere31(512,data,[pair_ipsi(1) pair_ipsi(2); pair_contra(1) pair_contra(2); all_comp_trans1],...
                                    'b',[8 12]);
    


%%
a = [1 3];
randi(a)

%%
% a = 0:1:10;
a = [];
figure;
tic
for i=1:10
    a = [a i]; 
    b = a + 2;
    c = a - 2;
    plot(a, b, a, c);
    xlim([0 10]);
    ylim([0 10]);    
    pause(1);
end
toc
%% plot.ly for matlab
https://plot.ly/matlab/getting-started/

x = 1:1:10;
y1 = sin(x);
y2 = cos(x);
fig = figure;
plot(x,y1, 'b--',x,y2, 'g--', 6, 0, 'r*');


%--PLOTLY--%
% Strip MATLAB style by default!
response = fig2plotly(fig, 'filename', 'matlab-basic-line');
plotly_url = response.url;

%% Interactive GUI
zeta = .5;                           % Damping Ratio
wn = 2;                              % Natural Frequency
sys = tf(wn^2,[1,2*zeta*wn,wn^2]);

f = figure;
ax = axes('Parent',f,'position',[0.13 0.39  0.77 0.54]);
h = stepplot(ax,sys);
setoptions(h,'XLim',[0,10],'YLim',[0,2]);

b = uicontrol('Parent',f,'Style','slider','Position',[81,54,419,23],...
              'value',zeta, 'min',0, 'max',1);
bgcolor = f.Color;
bl1 = uicontrol('Parent',f,'Style','text','Position',[50,54,23,23],...
                'String','0','BackgroundColor',bgcolor);
bl2 = uicontrol('Parent',f,'Style','text','Position',[500,54,23,23],...
                'String','1','BackgroundColor',bgcolor);
bl3 = uicontrol('Parent',f,'Style','text','Position',[240,25,100,23],...
                'String','Damping Ratio','BackgroundColor',bgcolor);
            
b.Callback = @(es,ed) updateSystem(h,tf(wn^2,[1,2*(es.Value)*wn,wn^2])); 

%%
pair_channels(iteration) = [pair_indexes];

a = [];
b = [1 2 nan 3 5];
a = [a; b];
figure;
plot(b, 'o--');
hold on
plot([1 1], [0 3], 'g:o','LineWidth', 1);
plot([2 2], [0 3], 'g:o','LineWidth', 1);
plot([3 3], [0 3], 'g:o','LineWidth', 1);
hold off


%% Interpolate EGI EEG system

% Load 1 meg
load('D:\data\stroke_dataset\006\filtdata_S2_bemcz_ellip2pass_1to20Hz.mat');
zeemg(meg.latency,meg.data);

% Add ref 204 to eeg
refAddedData = meg.data;
zeemg(meg.latency, refAddedData(:,160:end,:), meg.goodchannels(160:end));
refAddedData = cat(2, refAddedData, zeros(meg.srate, 1, size(meg.data, 3)));
zeemg(meg.latency, refAddedData(:,160:end,:), meg.goodchannels(160:end));

% Do the same with written function
refAddedData_fn = addReference204ToMegData(meg);

a = isequal(refAddedData, refAddedData_fn);

% Test isequal
refAddedData_false = meg.data;
refAddedData_false = cat(2, refAddedData, ones(meg.srate, 1, size(meg.data, 3)));
b = isequal(refAddedData, refAddedData_false);

% add back to meg
meg.data = refAddedData;

% Interpolate data
if( size(meg.goodchannels, 2) == 204 )
    continue
end

% Interpolate data
interpolated_meg = meg; 
egi_origine_chanloc_file_path = 'C:\Users\nguyendu\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\egi204.xyz';

eeglab_structure = convertStruct_megToEeglab( meg, egi_origine_chanloc_file_path );
bad_channels = getBadChannels( meg );
interpolated_eeglab_struct = electrodes_interpolation( meg, eeglab_structure, bad_channels, 1 ,1);
interpolated_meg.interpolated_data = convertEeg_eeglabToMeg(interpolated_eeglab_struct.data, original_meg.srate, size(original_meg.data, 3) );
interpolated_meg.filtered_rawdata_channels = interpolated_meg.goodchannels;
interpolated_meg.goodchannels = (1:1:length(interpolated_meg.sensor_labels))';


%% Check for the biosemi eeg in stroke_dataset
patient_data_dir = ('D:\data\stroke_dataset_biosemi\');
load('D:\data\stroke_dataset\ic_Precentral_LR.mat');
load('D:\data\stroke_dataset\side35pat.mat');

egi = 1:5;
biosemi1 = 6:21;
brainvision = 22:28;
biosemi2 = 29:35;

egi = zeros(size(1:5))
biosemi1 = zeros(size(6:21)) + 1;
brainvision = zeros(size(22:28)) + 2;
biosemi2 = zeros(size(29:35)) + 1;

alleeg = [egi biosemi1 brainvision biosemi2];
alleeg2 = [egi biosemi1 brainvision biosemi2];
alleeg = [alleeg alleeg2'];
alleeg = [alleeg tf2'];

biosemi = [biosemi1 biosemi2]';
biosemi = [biosemi index2'];


