% TESTING FUNCTIONS
clear;

%% Load path and file
% load([patient_data_path patient_data_filename]);
username = 'nguyendu'; %dunn or nguyendu
patient_data_path = 'FC_patients_data\P01\';
patients_data_path = 'FC_patients_data\';
patient_data_filename = 'filtdata_S_rest_pre_300_ellip2pass_1to20Hz';
origine_chanloc_file = 'biosemi128.xyz';
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\', origine_chanloc_file);
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);
data_dir = ('subjects_data\'); % 'FC_patients_data\' ou 'etude2_data\'
FMA = [23 17 22 17 46 15 18 19 54 29]'; % Fugl Meyer en pre pour chaque sujet
comps_path = 'data\COMPS';
load('data\pair_COMPS.mat');

%% TEST

% COMPS = get_COMPS_info(comps_path, origine_chanloc_file_path);

% getBadChannelsTest( patient_data_path,  patient_data_filename);

% convertEeg_megToEeglabTest( patient_data_path, patient_data_filename );

% convertStruct_megToEeglabTest( patient_data_path, patient_data_filename );

% convertEeg_eeglabToMegTest( patient_data_path, patient_data_filename );

% electrodes_interpolationTest( patient_data_path, patient_data_filename );

% interpolate_missing_channelsTest( patient_data_path, patient_data_filename );

% Get 128 channels combination files
% get_COMPS_infoTest( );

% Load all patients meg data test
% getAllPatientsMegDataTest( patients_data_path );

% Interpolating all patients meg structures
% allPatientsMeg_struct = getAllPatientsMegData(patients_data_path);
% allPatientsMeg_struct = addA1ReferenceToAllPatientsMegData(allPatientsMeg_struct);
% interpolate_all_megdataTest( allPatientsMeg_struct );
% close all;

% Permutation function test
% swapChannelsDataTest( patients_data_path, patient_data_path, patient_data_filename );

% Get xyz localizations and labels test
% getClosestChannelsStructTest();

% Re-reference with small Laplacian test
% reref_smallLaplacianTest(patient_data_path, patient_data_filename);

% Compare three found pairs with source code
% compareThreePairsWithSourceTest(patient_data_path, current_dir, comps_path, data_dir, origine_chanloc_file_path, COMPS, FMA);

% Find best 6 channels and control if follow constraints and rules
% getBestSixChannelsAndCorrelateTest( patient_data_path, current_dir, comps_path, data_dir, origine_chanloc_file_path, COMPS, FMA );

% Get all correlations for ipsilesional pairs
% getAllCorrelationsPerPairsTest(patient_data_path, current_dir, comps_path, data_dir, origine_chanloc_file_path, COMPS, FMA)

%% Work with future 19 channels 10-20

% Get 10-20 and biosemi labels correspondance
% get_1020Biosemi_19Channels_correspondance();

% Get 19 channels COMPS info
% get_19_channels_COMPS_info();

% Get all patients meg struct
% [ allSubjects_megStruct_interpoledNorerefNoswap, allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref ] = get_all_subjects_megstruct(current_dir, origine_chanloc_file_path);

% Work on subject data and 19 channels, take the reviewed file
work_on_subjectdata_19ch(allSubjects_megStruct_interpoledNorerefNoswap19ChAvgreref);

% Work on subject data and 19 channels
work_on_subjectdata_19chRerefSlap();


% Work on Anais patient's data and 19 channels
work_on_patientdata_19ch(inputArg1,inputArg2)



