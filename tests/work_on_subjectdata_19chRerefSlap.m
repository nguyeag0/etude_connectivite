function [] = work_on_subjectdata_19chRerefSlap()
%WORK_ON_SUBJECTDATA_19CHREREFSLAP Summary of this function goes here
%   Detailed explanation goes here

% Copy paste here and test with Slap on 19 electrodes.
% USER
username = 'nguyendu'; %dunn or nguyendu

% CHANNELS LOCATION FILE
origine_chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi128.xyz');
origine_19chanloc_file_path = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\data\biosemi19.xyz');

% CURRENT DIR
current_dir = strcat('C:\Users\', username, '\switchdrive\Documents\projects\3eEtude_connectivite\etude_connectivite\');
cd(current_dir);

% DATA DIR
data_dir = ('D:\data\Data_Anais_Leslie_Lea\'); % 'FC_patients_data\' ou 'etude2_data\' ou 'subjects_data\'
% data_dir = ('C:\data\subjects_data\');

% ALL 128 AND 19 PAIRS COMBINATIONS
comps_path = 'data\COMPS';
load('data\pair_COMPS.mat');
load('data\COMPS19.mat');

% To be correlated with
load('D:\data\Data_Anais_Leslie_Lea\NewPrecentral_values.mat'); % Precentral(:,1) --> Left  | Precentral(:,2) --> Right
% load('C:\data\subjects_data\Precentral_L_values.mat');
% load('C:\data\subjects_data\Precentral_R_values.mat');

%% LOAD DATA
% Get the set of patient EEG data, taking only 19 specific channels :
% Interpolated 1 | Re-ref 'no' | Swap 0
% [ ~, allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref ] = get_all_subjects_megstruct(data_dir, current_dir, origine_chanloc_file_path);

%---
% Get all subject MEG data : Interpolation 1 | Reref 'no' | Swap 0
allSubjects_megStruct_interpoledNorerefNoswap = getPatientsStructData(current_dir, data_dir, origine_chanloc_file_path); % all subject raw data
allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref = struct(allSubjects_megStruct_interpoledNorerefNoswap); % subject data reduced to 19ch

% Load the wanted channels and index
load('data/channels19_1020_biosemi.mat');
channels19_index = channels19_1020_biosemi.index;

% Get the 19 EEG channels and reref avg on 19 channels
for i = 1:length(allSubjects_megStruct_interpoledNorerefNoswap)
%     allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i) = allSubjects_megStruct_interpoledNorerefNoswap(i);    
    data19 = allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.data(:,channels19_index,:);
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.data19 = data19;
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.channels19 = channels19_index;    
    
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata.data = data19;
    megdata = allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata;
%     megdata = nut_eegref(megdata, 'AVG');
    megdata = eegref_smallLaplacian( megdata, origine_19chanloc_file_path );
    allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref(i).megdata = megdata;
end

%---

subject_number = length(allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref);
zScore_matrix = nan(length(COMPS19),subject_number);
ICmatrix = nan(length(COMPS19), subject_number);

% Get patient imaginary coherence and z-score based on 19 channels
for index=1:subject_number
    [ index_zScore, index_imaginary_coherence, subject_comps ] = get_index_zScore(allSubjects_megStruct_interpoledNorerefNoswap19ChSlapreref, index);
    ICmatrix(:,index) = index_imaginary_coherence;
    zScore_matrix(:,index) = index_zScore;
end

zScore_matrix_noswap = zScore_matrix;
zScore_matrix_swap = zScore_matrix;


%% Corr 

% corr
[rL_noswap,pL_noswap]=corr(zScore_matrix',Precentral(:,1),'type','spearman');
[rR_noswap,pR_noswap]=corr(zScore_matrix',Precentral(:,2),'type','spearman');

% All pairs (171)
selected_pairs = COMPS19(:,1:2); 
values = pL_noswap;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
title('171 Pairs: zscore with Precentral Left Values');

% pval < 0.05
selected_pairs = COMPS19(find(pL_noswap<0.05 ),1:2);    
values = pL_noswap(pL_noswap<0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [0 0.06];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['zscore with Precentral Left Values tail right' newline 'pval < 0.05  | color scale [0 0.06]'];
title(plot_title); 

% pL_noswap : swap to right side and compare
pL_SwappedToRight = pL_noswap;
rL_SwappedToRight = rL_noswap;
idnum = length(pL_SwappedToRight);
for i=1:idnum
    pL_SwappedToRight( COMPS19(i,9), 2) = pL_noswap(i,1);
    rL_SwappedToRight( COMPS19(i,9), 2) = rL_noswap(i,1);
end

stat_p_3D = cat(3, pL_SwappedToRight(:,2), pR_noswap);
p_binom = binomstat2('p', stat_p_3D, 0.05);
meanrr = tanh(mean(atanh([rL_SwappedToRight(:,2) rR_noswap]),2));
goodr = (p_binom<0.05);

values = meanrr(goodr);
selected_pairs = COMPS19(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['binomstat2 plot good pair'];
title(plot_title); 

%% Corr tail right

% corr
[rL_noswap,pL_noswap]=corr(zScore_matrix',Precentral(:,1),'type','spearman','tail','right');
[rR_noswap,pR_noswap]=corr(zScore_matrix',Precentral(:,2),'type','spearman','tail','right');

% All pairs (171)
selected_pairs = COMPS19(:,1:2); 
values = pR_noswap;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
title('171 Pairs: zscore with Precentral Right Values');

% pval < 0.05
selected_pairs = COMPS19(find(pR_noswap<0.05 ),1:2);    
values = pR_noswap(pR_noswap<0.05);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [0 0.06];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['zscore with Precentral Right Values tail right' newline 'pval < 0.05  | color scale [0 0.06]'];
title(plot_title); 

% pL_noswap : swap to right side and compare

pL_SwappedToRight = pL_noswap;
rL_SwappedToRight = rL_noswap;
idnum = length(pL_SwappedToRight);
for i=1:idnum
    pL_SwappedToRight( COMPS19(i,9), 2) = pL_noswap(i,1);
    rL_SwappedToRight( COMPS19(i,9), 2) = rL_noswap(i,1);
end

stat_p_3D = cat(3, pL_SwappedToRight(:,2), pR_noswap);
p_binom = binomstat2('p', stat_p_3D, 0.05);
meanrr = tanh(mean(atanh([rL_SwappedToRight(:,2) rR_noswap]),2));
goodr = (p_binom<0.05);

values = meanrr(goodr);
selected_pairs = COMPS19(goodr,1:2);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['binomstat2 plot good pair'];
title(plot_title); 

%% Stepwisefit

% stepwisefit with inverse_correlation_Zscore
[beta_preLeftVal,~,pval_preLeftVal,in_preLeftVal]=stepwisefit(zScore_matrix_noswap',Precentral(:,1));
[beta_preRightVal,~,pval_preRightVal,in_preRightVal]=stepwisefit(zScore_matrix_noswap',Precentral(:,2));

meanbeta_preLeftVal = mean(beta_preLeftVal);
meanbeta_preRightVal = mean(beta_preRightVal);

% all 171 pairs
% selected_pairs = COMPS_ind(:,1:2);    
selected_pairs = COMPS19(:,1:2);    
values = beta_preRightVal;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['171 Pairs' newline 'Beta with Precentral Right Values, mean beta = ' num2str(meanbeta_preRightVal)];
title(plot_title);   

% Pairs with beta <-0.08 or >0.08
selected_pairs = COMPS19(find(beta_preLeftVal>0.08 | beta_preLeftVal<-0.08 ),1:2);    
values = beta_preLeftVal(beta_preLeftVal>0.08 | beta_preLeftVal<-0.08 );
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
figure;
topoplot_connect(display_structure, origine_19chanloc_file_path); 
plot_title = ['Beta with Precentral Left Values, mean beta = ' num2str(meanbeta_preLeftVal) newline ...
            'beta <-0.08 or >0.08'];
title(plot_title);

% beta_preLeftVal: swap to right side and compare
pval_preLeftVal_SwappedToRight = pval_preLeftVal;

idnum = length(pval_preLeftVal_SwappedToRight);
for i=1:idnum
    pval_preLeftVal_SwappedToRight( COMPS19(i,9), 2) = pval_preLeftVal(i,1);
end


stat_p_3D = cat(3, pval_preLeftVal_SwappedToRight(:,2), pval_preLeftVal);
p_binom = binomstat2('p', stat_p_3D, 0.05);
% meanrr = tanh(mean(atanh([rL_SwappedToRight(:,2) rR_noswap]),2));
goodr = (p_binom<0.05);

end

