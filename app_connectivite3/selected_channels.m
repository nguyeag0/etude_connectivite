function varargout = selected_channels(varargin)
% SELECTED_CHANNELS MATLAB code for selected_channels.fig
%      SELECTED_CHANNELS, by itself, creates a new SELECTED_CHANNELS or raises the existing
%      singleton*.
%
%      H = SELECTED_CHANNELS returns the handle to a new SELECTED_CHANNELS or the handle to
%      the existing singleton*.
%
%      SELECTED_CHANNELS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SELECTED_CHANNELS.M with the given input arguments.
%
%      SELECTED_CHANNELS('Property','Value',...) creates a new SELECTED_CHANNELS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before selected_channels_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to selected_channels_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help selected_channels

% Last Modified by GUIDE v2.5 23-Jun-2019 11:44:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @selected_channels_OpeningFcn, ...
                   'gui_OutputFcn',  @selected_channels_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before selected_channels is made visible.
function selected_channels_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to selected_channels (see VARARGIN)

% Load Data
load('..\data\selected_channels_struct3.mat');
load('..\data\pair_COMPS.mat');
% handles.currentData = selected_channels_struct;
handles.pair_indexes_list = selected_channels_struct.pair_indexes_list;
handles.regr = selected_channels_struct.regressions;
handles.pair_COMPS = pair_COMPS;
handles.origine_chanloc_file_path = selected_channels_struct.origine_chanloc_file_path;
handles.vert_lines = selected_channels_struct.vert_lines;
handles.betas_list = selected_channels_struct.betas_list;
iter_number = length(handles.regr);
iterations = 1:1:iter_number;
handles.iterations = iterations';

% Set sliders parameters
set(handles.iteration_slider, 'Max', iter_number);
set(handles.iteration_slider, 'SliderStep', [1/iter_number 1/iter_number]);

% Set plot in figure area
selected_pairs = handles.pair_COMPS(handles.pair_indexes_list(1,:),:);    
values = handles.betas_list;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
topoplot_connect(display_structure, handles.origine_chanloc_file_path); 
plot_title = {['Iteration 1', ': regression = ', num2str(handles.regr(1))], ...
        ['Betas ', num2str(handles.betas_list(1)), ' ', num2str(handles.betas_list(2)), ' ', num2str(handles.betas_list(3))]};
title(handles.regr_topoplot, plot_title);   

% Set plot in figure area regr_topoplot  
plot(handles.regr_graph, handles.iterations, handles.regr, 'g:o',...
     handles.iterations(1), handles.regr(1), 'r*',...
     'LineWidth', 1);
xlim(handles.regr_graph, [1 iter_number]);
ylim(handles.regr_graph, [-1 1]);    
legend(handles.regr_graph,'regr');

% Choose default command line output for selected_channels
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes selected_channels wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = selected_channels_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function iteration_slider_Callback(hObject, eventdata, handles)
% hObject    handle to iteration_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

sliderVal = get(hObject, 'Value'); % sliderVal = get(handles.iteration_slider, 'Value') from outside this function
sliderVal = floor(sliderVal);
assignin('base', 'sliderVal', sliderVal);
set(handles.edit_iteration, 'String', num2str(sliderVal));

% Update channel_topoplot
handles.pair_indexes_list(sliderVal,:)
selected_pairs = handles.pair_COMPS(handles.pair_indexes_list(sliderVal,:),:);    
values = handles.betas_list;
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
topoplot_connect(display_structure, handles.origine_chanloc_file_path); 
plot_title = {['Iteration ', num2str(sliderVal), ': regression = ', num2str(handles.regr(sliderVal))], ...
        ['Betas ', num2str(handles.betas_list(1)), ' ', num2str(handles.betas_list(2)), ' ', num2str(handles.betas_list(3))]};    
title(handles.regr_topoplot, plot_title); 

% Update regr_graph
plot(handles.regr_graph, handles.iterations, handles.regr, 'g:o',...
     handles.iterations(sliderVal), handles.regr(sliderVal), 'r*',...
     'LineWidth', 1);
xlim(handles.regr_graph, [1 max(handles.iterations)]);
ylim(handles.regr_graph, [-1 1]);    
legend(handles.regr_graph,'regr');



% --- Executes during object creation, after setting all properties.
function iteration_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to iteration_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes just before selected_channels is made visible.
function iteration_slider_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to selected_channels (see VARARGIN)

handles.output = hObject;

% Initialization
set(handles.textNum, 'String', get(handles.sliderNum, 'Value'));
guidata(hObject, handles);


function edit_iteration_Callback(hObject, eventdata, handles)
% hObject    handle to edit_iteration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_iteration as text
%        str2double(get(hObject,'String')) returns contents of edit_iteration as a double
edit_iteration_value = get(hObject, 'String');
edit_iteration_value = floor(str2num(edit_iteration_value));

% Update channel_topoplot
handles.pair_indexes_list(edit_iteration_value,:)
selected_pairs = handles.pair_COMPS(handles.pair_indexes_list(edit_iteration_value,:),:);    
values = repmat(handles.regr(edit_iteration_value), 3, 1);
display_structure = struct();
display_structure.chanPairs = selected_pairs;
display_structure.connectStrength = values ;
display_structure.connectStrengthLimits = [-1 1];
topoplot_connect(display_structure, handles.origine_chanloc_file_path); 

plot_title = {['Iteration ', num2str(sliderVal), ': regression = ', num2str(handles.regr(sliderVal))], ...
        ['Betas ', num2str(handles.betas_list(1)), ' ', num2str(handles.betas_list(2)), ' ', num2str(handles.betas_list(3))]};    
title(handles.regr_topoplot, plot_title); 


%   Update corr_p_graph
plot(handles.regr_graph, handles.iterations, handles.regr, 'g:o',...
     handles.iterations(edit_iteration_value), handles.regr(edit_iteration_value), 'r*',...
     'LineWidth', 1);
xlim(handles.regr_graph, [1 max(handles.iterations)]);
ylim(handles.regr_graph, [-1 1]);    
legend(handles.regr_graph,'regr');


% --- Executes during object creation, after setting all properties.
function edit_iteration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_iteration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
